#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "Татарча укырга өйрәнәбез")
@(define-meta author           "")

@script[#:src "../../js/main.js"]

@(define AUDIOS "https://audio.selimcan.org/tat/")

@(define (transcribe t) (string-append "[" t "]"))

@(define (pronounce word) (string-append "«" word "»"))

@(define SOUNDS '("a" "ä" "ya" "yä" "b" "c" "ç" "d" "e" "yı" "ye" "f" "g" "ğ"
  "h" "i" "ı" "j" "k" "l" "m" "n" "ñ" "o" "ö" "p" "q" "r" "s" "ş" "t" "u" "ü"
  "yu" "yü" "v" "w" "x" "y" "z"))

@(for/splice ([s SOUNDS]) @audio[#:id s #:controls "" #:style "display: none"]{
  @source[#:src (string-append AUDIOS s ".wav") #:type
  "audio/ogg"] Браузерыгыз audio элементын аңламый.}  )

@p{Кирил әлифбасына нигезләнгән татар әлифбасы түбәндәгечә:}

@ul[#:class "alphabet"]{

 @li{А а}
 @li{Ә ә}
 @li{Б б}
 @li{В в}
 @li{Г г}
 @li{Д д}
 @li{Е е}
 @li{Ё ё}
 @li{Ж ж}
 @li{Җ җ}
 @li{З з}
 @li{И и}
 @li{Й й}
 @li{К к}
 @li{Л л}
 @li{М м}
 @li{Н н}
 @li{Ң ң}
 @li{О о}
 @li{Ө ө}
 @li{П п}
 @li{Р р}
 @li{С с}
 @li{Т т}
 @li{У у}
 @li{Ү ү}
 @li{Ф ф}
 @li{Х х}
 @li{Һ һ}
 @li{Ц ц}
 @li{Ч ч}
 @li{Ш ш}
 @li{Щ щ}
 @li{Ъ ъ}
 @li{Ы ы}
 @li{Ь ь}
 @li{Э э}
 @li{Ю ю}
 @li{Я я}

}

@p{Татарча укырга өйрәнергә теләүче бала өчен бу әлифбаның кыенлыгы ике
нәрсәдән гыйбарәт (тыңлау өчен хәрефкә басыгыз):}

@ol{

@li{хәрефләрнең кайберсе татар телендә үк берничә төрле аваз белдерә, ягъни «бер
хәреф — бер аваз» принцибы сакланмый

@numbered-note{Мәсәлән, е хәрефе өч аваз белдерә ала — @pronounce{тел}
дигәндәге @transcribe{e}, @pronounce{ел} дигәндәге @transcribe{yı},
@pronounce{егет} дигәндәге @transcribe{ye} (транскрипцияләрне 2012 нче елгы
латин әлифбасы аша бирдек).}

;}

@li{хәрефләрнең кайберсе татар телендә — бер, ә урыс телендә икенче бер авазны
белдерә, ә күп татар балалары бер үк вакытта урысча да укырга өйрәнгәнлектән,
бу буталчылык китереп чыгара

@numbered-note{Мәсәлән, ы: @pronounce{ылыс} vs @pronounce{рывок}, г:
@pronounce{гариза} vs @pronounce{гараж} һ.б.}

.}

}

@p{Бу ике фактор да татарча укырга өйрәнүне кыенлаштыра. Шундый хәл булмасын
өчен, без укырга өйрәнүчеләр өчен махсус, гадиләштерелгән әлифба әзерләдек. Бу
әлифбада, бер хәрефтән кала, барлык хәрефләр дә татар телендә бер генә аваз
белдерә ала. Моннан тыш, татар телендә — бер, ә урыс телендә икенче бер аваз
белдерүче хәрефләргә без махсус тамгалар өстәдек.}

@p{Шушы мәгънәдә гадиләштерелгән әлифба түбәндәгечә:

@margin-note{@hyperlink["https://audio.selimcan.org/tat/tatar-sounds.apkg"]{Әлифбаны
Anki пакеты буларак алу}}

}

@ul[#:class "alphabet"]{

 @li[#:onclick "hello_world.core.playSound('a')"]{\(\breve{а}\)}
 @li[#:onclick "hello_world.core.playSound('ä')"]{\(\ddot{а}\)}
 @li[#:onclick "hello_world.core.playSound('ä')"]{ә}
 @li[#:onclick "hello_world.core.playSound('b')"]{б}
 @li[#:onclick "hello_world.core.playSound('w'); hello_world.core.playSound('v')"]{в}
 @li[#:onclick "hello_world.core.playSound('g')"]{г}
 @li[#:onclick "hello_world.core.playSound('ğ')"]{\(\breve{г}\)} 
 @li[#:onclick "hello_world.core.playSound('d')"]{д}
 @li[#:onclick "hello_world.core.playSound('e')"]{\(\breve{е}\)}
 @li[#:onclick "hello_world.core.playSound('yı')"]{\(\bar{е}\)}
 @li[#:onclick "hello_world.core.playSound('ye')"]{\(\ddot{е}\)}
 @li[#:onclick "hello_world.core.playSound('j')"]{ж}
 @li[#:onclick "hello_world.core.playSound('c')"]{җ}
 @li[#:onclick "hello_world.core.playSound('z')"]{з}
 @li[#:onclick "hello_world.core.playSound('i')"]{и}
 @li[#:onclick "hello_world.core.playSound('y')"]{й}
 @li[#:onclick "hello_world.core.playSound('k')"]{к}
 @li[#:onclick "hello_world.core.playSound('q')"]{\(\breve{к}\)} 
 @li[#:onclick "hello_world.core.playSound('l')"]{л}
 @li[#:onclick "hello_world.core.playSound('m')"]{м}
 @li[#:onclick "hello_world.core.playSound('n')"]{н}
 @li[#:onclick "hello_world.core.playSound('ñ')"]{ң}
 @li[#:onclick "hello_world.core.playSound('o')"]{\(\breve{о}\)} 
 @li[#:onclick "hello_world.core.playSound('ö')"]{ө}
 @li[#:onclick "hello_world.core.playSound('p')"]{п}
 @li[#:onclick "hello_world.core.playSound('r')"]{р}
 @li[#:onclick "hello_world.core.playSound('s')"]{с}
 @li[#:onclick "hello_world.core.playSound('t')"]{т}
 @li[#:onclick "hello_world.core.playSound('u')"]{у}
 @li[#:onclick "hello_world.core.playSound('ü')"]{ү}
 @li[#:onclick "hello_world.core.playSound('f')"]{ф}
 @li[#:onclick "hello_world.core.playSound('x')"]{х}
 @li[#:onclick "hello_world.core.playSound('h')"]{һ}
 @li[#:onclick "hello_world.core.playSound('ts')"]{ц}
 @li[#:onclick "hello_world.core.playSound('ç')"]{\(\breve{ч}\)} 
 @li[#:onclick "hello_world.core.playSound('ş')"]{ш}
 @li[#:onclick "hello_world.core.playSound('şç')"]{щ}
 @li[]{ъ}
 @li[#:onclick "hello_world.core.playSound('ı')"]{\(\breve{ы}\)} 
 @li{ь}
 @li[#:onclick "hello_world.core.playSound('e')"]{э} 
 @li[#:onclick "hello_world.core.playSound('yu')"]{\(\bar{ю}\)}
 @li[#:onclick "hello_world.core.playSound('yü')"]{\(\ddot{ю}\)}
 @li[#:onclick "hello_world.core.playSound('ya')"]{\(\bar{я}\)}  
 @li[#:onclick "hello_world.core.playSound('yä')"]{\(\ddot{я}\)}  

}

@p{Әлеге әлифбадагы һәр хәрефнең нинди аваз белдерүен 2012-нче елгы латин
әлифбасы аша күрсәтсәк, транскрипцияләр менә болай булыр:}

@table[#:class "alphabet-table"
 @tr[@th{Безнең әлифба} @th{2012 елгы латин әлифбасы}]
 @tr[@td{\(\breve{а}\)} @td{a}]
 @tr[ @td{\(\ddot{а}\)} @td{ä}]
 @tr[@td{ә}             @td{ä}]
 @tr[@td{б} @td{b}]
 @tr[@td{в} @td{w яки v}]
 @tr[@td{г} @td{g}]
 @tr[@td{\(\breve{г}\)} @td{ğ}]
 @tr[@td{д} @td{d}]
 @tr[@td{\(\breve{е}\)} @td{e}]
 @tr[@td{\(\bar{е}\)} @td{yı}]
 @tr[@td{\(\ddot{е}\)} @td{ye}]
 @tr[@td{ж} @td{j}]
 @tr[@td{җ} @td{c}]
 @tr[@td{з} @td{z}]
 @tr[@td{и} @td{i}]
 @tr[@td{й} @td{y}]
 @tr[@td{к} @td{k}]
 @tr[@td{\(\breve{к}\)} @td{q}]
 @tr[@td{л} @td{l}]
 @tr[@td{м} @td{m}]
 @tr[@td{н} @td{n}]
 @tr[@td{ң} @td{ñ}]
 @tr[@td{\(\breve{о}\)} @td{o}]
 @tr[@td{ө} @td{ö}]
 @tr[@td{п} @td{p}]
 @tr[@td{р} @td{r}]
 @tr[@td{с} @td{s}]
 @tr[@td{т} @td{t}]
 @tr[@td{у} @td{u}]
 @tr[@td{ү} @td{ü}]
 @tr[@td{ф} @td{f}]
 @tr[@td{х} @td{x}]
 @tr[@td{һ} @td{h}]
 @tr[@td{ц} @td{ts}]
 @tr[@td{\(\breve{ч}\)} @td{ç}]
 @tr[@td{ш} @td{ş}]
 @tr[@td{щ} @td{şç}]
 @tr[@td{ъ} @td{_}]
 @tr[@td{\(\breve{ы}\)} @td{ı}]
 @tr[@td{ь} @td{_}]
 @tr[@td{э} @td{e}]
 @tr[@td{\(\bar{ю}\)} @td{yu}]
 @tr[@td{\(\ddot{ю}\)} @td{yü}]
 @tr[@td{\(\bar{я}\)} @td{ya}]
 @tr[@td{\(\ddot{я}\)} @td{yä}]

]

@; TODO x sounds in total. y only in Russian loan words.

@p{Укырга өйрәнүче эшне шушы махсус әлифбадагы хәрефләр белән, алар нинди аваз
белдерүләре белән танышудан башлый. Курс барышында хәрефләргә өстәлгән ярдәмчел
тамгалар әкренләп кими барып, укучы әкренләп гади татар әлифбасында язылган
текстларны укуга күчә.}

@p{Хәрефләрнең барысын да регуляр итеп булган булыр иде, әмма тәҗрибә алай
эшләүнең дөрес түгеллеген күрсәтә -- әлифбаны тулысынча регуляр итсәң, регуляр
булмаган, «чын» әлифбага күчкәндә кыенлыклар туарга мөмкин. Шунлыктан «в»
хәрефен ике аваз белдерә торган итеп калдырдык. «Вагон» я «ваза» кебек алынма
сүзләрдә бу хәреф [v] авазын (яңгырау ирен-теш апроксиманты), башка сүзләрдә
исә [w] авазын (яңгырау лабиовеляр апроксимант) белдерә.}

@;TODO a note on ё , saying that it was deliberate left ambiguous between Tatar
@;and Russian to introduce the kid to the idea that different letters can mean
@;different souns in different languages.

@p{Кечерәк шрифт белән язылган хәрефләр укылмыйлар, мисалы «ь» һәм «ъ».}

@h2{Беренче дәрес}

@h3{Беренче бирем. Яңа аваз белән танышу}

@ol{

@li{@ins{@sound{а}'га төртеп күрсәтегез.} @say{Мин бармагымны бу аваз астына
куеп, аны әйтеп күрсәтәчәкмен.} @ins{Җәянең беренче түгәрәгенә төртеп
күрсәтегез. Тиз генә икенче түгәрәккә күчегез. Ике секунд тотып торыгыз.}
@say{ааа.} @ins{Бармагыгызны җәя буйлап йөртеп бетерегез.}}

@li{@say{Хәзер синең чират. Мин аңа төртеп күрсәткәч, авазны әйт.}
(Бармагыгызны беренче түгәрәккә куегыз.) @say{Әзерлән.} (Икенче түгәрәккә
күчегез. Тотып торыгыз.) @say{«\(\breve{а}\)\(\breve{а}\)\(\breve{а}\)»}}

@li{(Беренче түгәрәкне төртеп күрсәтегез.) @say{Тагын бер кат. Әзерлән.} (Тиз
генә икенче түгәрәккә күчегез. Туктап торыгыз.)
@say{«\(\breve{а}\)\(\breve{а}\)\(\breve{а}\)»}}

}

@img[#:src "../../img/a-back.png" #:alt "back vowel a"]
