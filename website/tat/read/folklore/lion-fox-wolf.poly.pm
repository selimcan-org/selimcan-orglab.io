#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@(require (rename-in (only-in "texts/lion-fox-wolf.tat.pm" doc)
           (doc TAT)))
@(require (rename-in (only-in "texts/lion-fox-wolf.tur.pm" doc)
           (doc TUR)))

@(for/splice ([tat (cdr TAT)]
              [tur (cdr TUR)])
 @div[#:class "page"]{
  @div[#:class "lefthalf"]{@tat}
  @div[#:class "righthalf"]{@tur}})

@h3{Чыганаклар / Kaynaklar}

@ol{

@li{Гатина, Ярми (1977) Халык авыз иҗаты. Беренче том. Әкиятләр}

@li{Mustafa Gültekin (2010) Tataristan masallari üzerinde bir araştırma. T.C.
Ege üniversitesi sosyal bilimler enstitüsü Türk Halk Bilimi Anabilim dalı
(doktora tezi)}

}
