#lang pollen

@h3{Kurnaz tilkl [2]}

@p{Günlerden bir gün tilki, porsuk, kurt, deve ve at dost olmuşlar. Koca bir
çayırda, bütün yaz rahat bir şekilde yaşamışlar. Güz gelince, soğuk rüzgarlar,
esmeye başlamış. Porsuk, tilki ve kurt birleşip, at ile deveyi kesmeye karar
vermişler. Tilki, deveye:}
       
@p{— Sen kışı çıkaramazsın, senin kışı çıkarman zor olur, yiyecek bulmak da
zorlaştı, demiş.}

@p{Deve:}

@p{— Çok doğru söylüyorsun, aramızda hangimiz daha genç ise, önce onu keseriz,
demiş.}

@p{Sonra bunlar, toplanmışlar ve “Hangimiz yaşlı, hangimiz genç?” diye
tartışmaya başlamışlar. Deve:}

@p{— Ben, sizin hepinizden de büyüğüm; ben cennetten Adem ile birlikte çıktım,
demiş.}

@p{Tilki:}

@p{— Ama ben, Nuh peygamberin gemisinde çocukları okutmuş biriyim, ben hocayım,
demiş.}

@p{Kurt:}

@p{— Ben de Nuh peygamberin gemisinde bekçilik yaptım, düşmanları Nuh
peygamberin gemisinin yakınına getirmedim, demiş.}

@p{Porsuk:}

@p{— Ben doktorum, benim yağım türlü türlü hastalıklara şifadır. Nuh’un
gemisinde ben pek çok insanı iyileştirdim, demiş.}

@p{Bunlar ata sormuşlar: — Sen kaç yaşındasın? demişler.}

@p{— Yaşım hakkında benim belgem var, demiş. Eğer inanmıyorsanız, işte, tilki
hocadır, o okur, demiş.}

@p{Tilki:}

@p{— Nerede öyleyse senin o belgen? diye sormuş.}

@p{Benim belgem, toynağımın altında, demiş at.}

@p{Tilki, kurda:}

@p{— Eğer, sen bunun art ayağını kaldırıp, tutarsan, ben orada yazanları
okurum, demiş. Kurt, atın yanına gelip, art ayağını tutmasıyla birlikte, at
kurdu tepmiş, kurt sendeleyip, yere düşmüş. At:}

@p{— Şimdi benim kaç yaşında olduğumu öğrendiniz mi! deyip, çekip gitmiş.}

@p{Bunlar, dördü birlikte kalmışlar. Havalar iyice soğumaya başlamış. Porsuk,
deveye:}

@p{— Kış soğuk olur, sen kış boyunca boş yere acı çekme, biz seni keselim,
demiş. Ben doktorum, sonra ben seni diriltmeyi üzerime alırım, demiş. Gelecek
yaza biz seninle rahatça yaşarız, demiş.}

@p{Deve, tilki ile istişare etmiş:}

@p{— Öyle mi, bu porsuğun sözü doğru mu acaba? demiş.}

@p{Tilki:}

@p{— Niye doğru olmasın; o, doktor, diriltir, demiş.}

@p{Tilki, devenin dilinin altındakini sezip, hemen gür bir ağacın yanına
gitmiş, orada derin bir çukur kazıp, kendine yuva yapmış. Havalar soğuyup,
yiyecek bir şey bulamamaya başlayınca deve, kesilmeye razı olmuş. Deveyi
kesmişler. Deveyi kestikten sonra, kurt: — Bunun, yüreğini, bağırsaklarını,
başını ırmağa götürüp, yıkayıp, gelin, demiş.}

@p{Tilki ile porsuk bağırsakları yıkamaya gitmişler. Kurt, kalmış şimdi; deve
etlerini korumak için. Tilkinin karnı çok acıkmış. Tilki, porsuğa:}

@p{— Benim karnım acıktı, ben bunun başını beynini yiyeyim, demiş ve yemiş
bitirmiş. Şimdi tilki, porsuğa:}

@p{— Eğer, senin de karnın açsa, sen devenin yüreğini ye, fakat kurda, ben
cevap vereyim, demiş. Porsuk, devenin yüreğini yemiş. Bundan sonra, tilki
porsuğa:}

@p{— Kurt bizden, “Yürek nerede?” diye sorarsa, sen bana bırak, ben cevabını
veririm, demiş. Bunlar yemekten kalan artıkları alıp, kurdun yanına gitmişler.}

@p{Kurt:}

@p{— Bunun, başı ile beyni nerede? demiş.}

@p{Tilki:}

@p{— Onun beyni çoktan kuruyup, bitmiş; başında beyni olsa o kendi kendini
kestirmezdi, demiş.}

@p{— Öyleyse, yüreği nerede? demiş kurt. Porsuk, tilkiye dönüp bakmış. Tilki
gözünü kırpmadan:}

@p{— Bak hele, niye sen bana dönüp, bakıyorsun, yüreği yediğin zaman
bakmıyordun, demiş.}

@p{Kurt çok sinirlenip, porsuğun üzerine atılmış. Porsuk, kurttan korkup, hemen
kaçmaya çalışınca. Tilki bunu görüp:}

@p{— Tut, tut hırsızı, deve yüreğini o yiyip bitirdi, diye bağırmış.}

@p{Kurt ile porsuk gözden uzaklaşınca, tilki, deve etinin en güzel yerlerini
seçip, kendi yuvasına götürüp, koymuş. Geride toynaklar, kemikler, deve etinin
en kötü yerleri, kalmış. Etin en güzel yerlerini taşıyıp, bitirince tilki
rahatça, kendi yuvasına girip, yatmış.}

@p{Kurt dönüp, etin yanına gelmiş. Bakmış ki etin kötü yerleri, kemikleri
kalmış.  “Ah, hain! Beni kandırdı bu tilki, ben o tilkiyi görürsem hemen onu
parçalarım.” demiş.}

@p{Kurt artan parçaları bir hafta içinde yiyip bitirmiş, kış boyunca aç kalmış,
fakat kurnaz tilki kış boyunca et yiyip, kışı rahat geçirmiş.}

@p{İkinci güz gelmiş. Tilki bir köye gitmiş ve o köyden çeşitli kitap
parçalarını alıp, gelmiş. Gözlük takıp, kitap okur gibi yaparak, bir kütüğün
tepesine oturmuş.  Bunun yanına kurt çıkıp, gelmiş. “Ah, hain! demiş, geçen
sene sen beni aldatmıştın, bunun için şimdi ben seni parçalarım.” demiş.}

@p{— Ey, ahmak kurt, sen yanılıyorsun, ben seni hiçbir zaman görmedim. Ben
öğretmenim; bütün hayvanların yavrularını okutuyorum, eğer senin de yavruların
varsa, getir, ben onları da okuturum, demiş.}

@p{Kurt:}

@p{— Benim de iki yavrum var, demiş.}

@p{— Öyleyse, demiş tilki, yavrularını getir, yavru başına da iki koyun
getirirsin, demiş. Az sonra, bütün hayvanların yavruları gelirler, sen
yavrularını onlardan önce getir, önde gelenlere yer var, önceden gelip yer
bulsunlar, demiş.}

@p{Kurt geri dönmüş. İki gün sonra kurt, dört koyun gövdesi ile iki yavrusunu
getirmiş ve tilki hocaya vermiş. Tilki bunun getirdiği etleri de, yavrularını
da kabul edip, almış ve:}

@p{— Altı ay dolunca yavrularının durumunu öğrenmeye gel, demiş.}

@p{Kurt buna hediyeleri verip, yavrularını bırakıp, geri dönmüş. Kurdun
gitmesiyle tilki bunun iki yavrusunu da kesip, iki ayda yiyip, bitirmiş ve
kemiklerini derilerine sarıp, köşeye koymuş. Altı ay dolunca dört koyun
gövdesini yiyip bitirmiş ve tilki oradan sıvışmış.}

@p{Kurt, altı ay dolunca yavrularını görmeye gelmiş. Gelip bakmış ki tilki de,
çocukları da yok, onların yerinde yeller esiyor! Kurt, tilkinin yuvasını bulup,
onun içine bir bakınca köşede yavrularının derilerini görmüş. “Ah, durum kötü,
demiş, bu benim yavrularımı yiyip, bitirmiş, demiş. Eğer bu tilki bana rast
gelirse, ben onu itirazsız öldürürüm” demiş.}

@p{Yaz gelmiş, havalar ılımaya, sular coşmaya başlamış. Köyün dışında bir
değirmen varmış. O değirmenin kapısı açık kalmış ve tilki oraya girip, un
yalayıp duruyormuş. O sırada değirmene bizim kurt gelip, girmiş. Tilkiyi
görüp:}

@p{— Ah, hain tilki! demiş kurt, rast geldin mi, demiş, sen beni iki kere
aldattın, bu kez aldatamazsın, ben seni şimdi boğup, öldüreceğim, demiş.}

@p{— Vay ahmak kurt, ben seni hiç görmedim. Ben işte şu değirmende yıllardır
değirmencilik yapıyorum. Karnın aç mı? Karnın açsa şimdi ben sana un vereyim,
demiş.}

@p{Kurt, bunun sözüne inanıp:}

@p{— Öyle mi? Öyleyse tamam. Karnım çok aç, sen bana un ver, demiş.}

@p{Tilki bir kap un alıp, onu değirmen taşının üstüne incecik serpiştirerek,
dağıtmış.}

@p{Kurdun karnı çok açmış ve hiçbir şey düşünmeden değirmen taşının üstüne
oturup, unu yalamaya başlamış. Kurt unu yaladığı sırada tilki, hemen gidip,
değirmenin oluğunu @numbered-note{Кавыз — su değermeni oluğu.} açmış. Suyun
akmasıyla değirmen taşı çok hızlı dönmeye başlamış. Taşın dönmesiyle kurt,
ileri atılıp, değirmenin duvarına çarpmış. Kurt çok kötü yaralanmış, “of, of!”
diye yatarken o sırada tilki çıkıp, kaçmış.}

@p{Bir süre sızlanıp, yattıktan sonra kurt, kendine gelip, ayağa kalkmış. “Hele
bunun benim yavrularımı yemesi yetmemiş, beni de harap ede yazdı, demiş,
yeniden rast gelmez mi acaba bu, demiş, rast gelse hak ettiği cezayı verirdim
ben ona, demiş.}

@p{Günlerden bir gün tilki bir kucak dal, çubuk toplayıp, bir dağın eteğinde
türkü söyleye söyleye sepet örmeye çalışıyormuş. Türkü söylerken sesini işiten
kurt, tilkinin yanına gitmiş.}

@p{Kurt, tilkiyi tanıyıp:}

@p{— E, kurnaz tilki yakalandın mı? Şimdi ben seni bir vuruşta yok edeyim mi?
demiş.}

@p{— Ey, yiğit hayvan, demiş tilki. Niye sen beni yemek istiyorsun, senin bana
nasıl bir öfken var? demiş.}

@p{Kurt:}

@p{— Niçin öfkem olmasın, sen beni pek çok defa aldattın, demiş.}

@p{— Eh, ahmak kurt, demiş tilki. Benim yıllardır bir yere gittiğim yok. Ben
yıllardır bu dağ başında sepet örüp, yaşıyorum. Sen de olmayanı hayal edip,
durma, sepet örmeyi öğren. Seninle sepet örüp, pazarda satarız. Karın da tok
olur, kaygı da yok olur, demiş.}

@p{Bundan sonra tilki sepetin dibini yapmış ve o sepetin dibine kurdu oturtup,
kendisi sepetin etrafını örmeye başlamış. Sürekli: “Sen benim nasıl ördüğüme
bakıp dur, öğrenince sen de şöyle güzel örmeye başlarsın” demiş. Kurt bunun
sözünü dinleyip, buna inanmış.}

@p{Tilki sepeti örerken, kurt “Ben de sepet örmeyi öğreneyim” deyip, onun
örüşüne bakıp, oturuyormuş. Öre öre tilki epeyce büyük örmüş. Kurdun başından
yukarı tarafı daralta daralta çıkıp, sepeti kurdun çıkamayacağı şekilde örüp,
bitirmiş.}

@p{Bundan sonra tilki:}

@p{— Tamam, kurt kardeş sağ ol! demiş ve sıvışmış.}

@p{Kurt:}

@p{— Ah, seni hain, deyip, tilkinin ardından kovup gitmek isteyince sepetten
çıkamamış. Çok da geçmeden pazara gidenler kurdu öldürüp arabaya koyup,
gitmişler.  Kurt bitmiş, kurnaz tilki kendi yoluna gitmiş.}
