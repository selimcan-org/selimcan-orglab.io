#lang pollen

@h3{Aslan, tilki ve kurt [2]}

@p{Bir zamanlar aslan, tilki ve kurt avlanmaya çıkmışlar. Bir kır eşeği, bir
kır keçisi ve bir porsuk yakalamışlar ve bunları aralarında paylaşmaya
oturmuşlar. Aslan, kurda: “Üçümüze paylaştır bunları” demiş. Kurt kendine kır
eşeğini alıp: “Bu benim.”  demiş. Aslana kır keçisini vermiş ve “Bu aslan
hazretlerine” demiş. Tilkiye de porsuğu vermiş. Aslan buna kızıp, bütün gücüyle
kurdun başına vurup, kurdun başını yarmış ve sonra tilkiye:}

@p{“Sen paylaştır!” demiş. Tilki paylaştırmaya çalışmış. Kır eşeğini, aslana
vermiş: “Bunu erkenden ye.” demiş. Kır keçisini vermiş: “Bunu gündüz ye.”
demiş.  Porsuğu vermiş: “Bunu da gece ye” demiş. Üçünü de aslana vermiş.}

@p{Aslan: “Allah mübarek etsin, bu miras paylaştırma ilmini sana kim öğretti?”
demiş. Tilki kurdu gösterip: “Bunu başına kızıl börk giyen öğretti.” demiş.}
