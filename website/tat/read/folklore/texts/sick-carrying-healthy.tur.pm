#lang pollen

@h3{Sağlıklıyı hasta taşır [2]}

@p{Hayvanlar padişahı aslan hastalanmış, yatıyormuş. Bütün hayvanlar, aslanın
durumunu öğrenmeye gelmiş, ama hilekâr tilki gelmemiş. Tilki gelmeyince kurt,
aslana gidip, şikayet etmiş, “O sana saygı göstermiyor”, demiş. Kurdun şikayet
ettiğini tilki bir kenardan seyrediyormuş.}

@p{Bir süre sonra tilki, aslanın yanına gelince, aslan gerinerek:}

@p{— Ey hain, sen neden böyle sona kaldın? demiş.}

@p{— Ben senin hasta olduğunu duydum da sana ilaç aradım, demiş tilki.}

@p{— Peki, ilaç buldun mu? diye sorunca aslan;}

@p{— Buldum, kurdun art bacağını kesip yersen, senin hastalığın iyileşir,
demiş.}

@p{Aslan, o anda kurdun üzerine atılmış ve arka bacağının bir kısmını koparıp,
yemiş.}

@p{Artık kurt aksayarak yürüyormuş. Dolaşırken tekrar tilkiye rastlamış:}

@p{— Benim işim kötü, demiş, ayağım ağrıyor, bir çare bilmiyor musun? demiş.}

@p{— Biliyorum, demiş, kuyruğuna sandık bağlayıp buz deliğine sokup oturursan,
balıklar yakalanır, demiş. O balıkları yersen, ayağın düzelir, demiş.}

@p{Kurt; tilkinin öğrettiği gibi kuyruğuna bağladığı bir sandığı buz deliğine
sokup oturmuş. Kuyruğu çıtır çıtır edince, Tilki: “Dokunma dokunma biraz dursun
hele, çokça girsin” demiş.}

@p{Sabahleyin kadınlar suya gelmişler, kuyruğu suya batan kurdu görüp, su
terazileri ile dövmeye başlamışlar. Dövmeye başlayınca, kurt kuyruğunu koparıp
kaçmış.}

@p{Dolaşırken, dolaşırken yine aynı tilkiye rastlamış.}

@p{— İşim çaresiz, demiş kurt, ayağım ağrıyor, kuyruğum da koptu, demiş, sen
bir çare bilmiyor musun? demiş.}

@p{— Biliyorum, demiş, şu dağın başına çıkarsan, orada ilaç bulursun, demiş.}

@p{— Sen bana göster şimdi o ilacı, demiş.}

@p{—Gösterirdim de, demiş tilki, benim vaktim yok, şimdi ben senin üstüne
otursam, çabucak varıp gelsek, demiş.}

@p{— Haydi otur üstüme, demiş, tez varıp dönelim, demiş.}

@p{Tilki, kurdun üstüne oturunca: “Sağlıklıyı hasta kaldırır, sağlıklıyı hasta
kaldırır.” diye türkü söylemeye başlamış.}

@p{Şimdi de böyle türkü söyleye söyleye ilaç arayıp duruyorlarmış.}
