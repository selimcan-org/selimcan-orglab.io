#lang pollen

@h3{Ağıl yapn tilki}

@p{Bir gün aslan, tavuk yetiştirmeye karar vermiş. Ancak tavukları az
çoğalıyormuş. Şaşılacak şey değil, çünkü onlar çok serbestmiş; hangisi nereye gitmiş,
hangisini çalmışlar, hangisi kaybolmuş… Aslan bu zararlardan kurtulmak için ağıl
yaptırmayı düşünmüş ve çok büyük bir ağıl yaptırmış. Yeme içme yerleri de
içindeymiş, konaklama yeri de, yürüme yeri de içindeymiş. Böyle olmasına rağmen
tavuklar hiç artmıyormuş.
Bir gün aslana gelip, tilki ağıl yapmakta çok usta, demişler. Aslan bu haberi
alınca, tilkiyi çağırtıp: “Şöyle şöyle işittim, mümkünse, rica etsem yapıverseniz ya.”
demiş. Tilki; “Baş üstüne” demiş; aslanın sözünü kabul edip, canhıraş ağıl yapmaya
başlamış ve tez zamanda ağılı tamamlamış.
Ağıla bakmışlar. Duvarları çok büyükmüş, yeme içme yerleri hazırmış, uyuma
yerleri farklıymış, yumurta salınan yerleri kendi altındaymış, hiç zarar gelmeyecek
şekilde yapılmış. Aslan, ağılı beğenip, tilkiye pek çok hediye vermiş. Yine de her gün
bir tavuk kayboluyormuş. Bu işe şaşırıp, aslan bekçi koymuş. Bekçi koyunca, bu ağılı
yapan tilki yakalanmış. Bakmışlar ki, o çalıyormuş. Ağılı yaptığında, hiç kimseye fark
ettirmeden, kendisi girecek kadar bir delik bırakmış.}
