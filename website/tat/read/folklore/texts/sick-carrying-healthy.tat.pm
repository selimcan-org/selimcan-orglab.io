#lang pollen

@h3{Сауны хаста күтәрер [1]}

@p{Хайваннар патшасы Арслан авырып ята икән. Арсланның хәлен белергә бөтен
хайваннар килеп беткәннәр, ди, ә хәйләкәр Төлке һаман юк, ди. Төлке килмәгәч,
Бүре Арсланга барып әләкләгән, ул сине санламый, дип. Бүренең әләкләгәнен Төлке
бер кырыйдан гына карап торган икән.}

@p{Бервакытны Төлке килеп керүгә Арслан ачуы килеп гайрәтләнеп янына килә дә:}

@p{— Әй, хаин, син нигә болай соңга калдың? — ди.}

@p{— Мин синең авырганыңны ишеттем дә сиңа дару эзләдем, — ди.}

@p{— Соң, дару таптыңмы? — ди.}

@p{— Таптым, ди, Бүренең арт сыйрагын кисеп ашасаң, синең авыруың бетәр, — ди.}

@p{Арслан шунда ук Бүрегә ташлана да арт сыйрагын йолкып ала.}

@p{Хәзер Бүре аксап китә инде. Йөри торгач, тагын Төлкегә очрый бу.}

@p{— Минем эш харап, ди, аягым авырта, берәр дару белмисеңме, — ди.}

@p{— Беләм, ди, койрыгыңа кызау бәйләп бәкегә тыгып утырсың, балыклар эләгер,
ди. Шул балыкларны ашасаң, аягың төзәлер, — ди.}

@p{Ярар, Төлке өйрәткәнчә, койрыгына кызау бәйләп бәкегә тыгып утыра
бу. Койрыгы шытыр-шытыр килгән саен Төлке әйтә: «Тимә, тимә бераз торсын әле,
күбрәк керсен», — ди.}

@p{Иртә белән хатыннар суга килсәләр, койрыгы бозга каткан Бүрене күреп,
көянтәләр белән кыйный башлыйлар. Кыйный торгач, Бүре койрыгын өзеп кача, ди.}

@p{Йөри-йөри тагын әлеге Төлкегә очрый бу.}

@p{— Эшем начар, — ди Бүре, — аягым авырта, койрыгым да өзелде, ди, син берәр
дару белмисеңме? — ди.}

@p{— Беләм, ди, әнә шул тау башына менсәң, шунда дару табарсың, — ди.}

@p{— Син миңа күрсәт инде ул даруны, ди.}

@p{— Күрсәтер идем дә, — ди Төлке, — минем вакытым юк, ди. Инде өстеңә утыртып,
тиз генә барып кайтсак кына, — ди.}

@p{— Әйдә утыр минем өстемә, ди, тиз генә барып кайтыйк, — ди.}

@p{Төлке Бүре өстенә утырган да, ди: «Сауны хаста күтәрер, сауны хаста
күтәрер», — дип җырлап бара, ди.}

@p{Әле дә булса шулай җырлый-җырлый дару эзләп йөриләр, ди, болар.}
