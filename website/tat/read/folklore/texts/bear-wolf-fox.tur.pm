#lang pollen

@h3{Ayı, kurt ve tilki [2]}

@p{Bir vakitler ayı, kurt ve tilki kendi aralarında konuşup, bir karara
varmışlar: “Biz, hacca gidip gelelim, bundan sonra da bir kimseye dokunmayalım,
kendi gücümüz ile gün geçirip yaşayalım.” demişler ve hiç kimseye zarar
vermemeye de söz vermişler.}

@p{Bunlar yola çıkıp, gitmişler. Yolda bir deveye rastlamışlar. Deve bunlara:}

@p{— Nereye gidiyorsunuz, arkadaşlar? diye sormuş.}

@p{Bunlar:}

@p{— Biz hacca gidiyoruz ve bundan böyle hiç kimseye de zarar vermemeye söz
verdik, demişler.}

@p{Deve:}

@p{— Tamam, öyleyse beni de götürseniz ya, demiş.}

@p{Bundan sonra, bunlar birlikte yola çıkmışlar. Birkaç gün gitmişler. Yolda
deve, ot yiyip karnını doyurmuş. Ot yemek günah değil ne de olsa. Ayı, kurt ve
tilki gittikçe acıkmış, acıktıkça deveye kin duymaya başlamışlar: “Deve
beslenip duruyor, haydi biz bunu öldürelim” demişler. Çok geçmeden bunlar üçü
birlikte deveyi boğup, öldürmüşler. Yüzdükten sonra, ayı: “Ben bunun içini,
karnını gölde yıkayıp, geleyim; ben dönene kadar siz hiç bir şeye dokunmayın.”
demiş.}

@p{Bu gitmiş. Ayının gitmesiyle tilki, kurda: “Haydi, kurt kardeş, ayı dönene
kadar biz bunun böbreğini yiyelim, ben ona cevap veririm; fakat o dönüp,
“Böbrek nerde?” diye sorarsa, sen bana dönüp, bak, demiş.}

@p{Ayı dönmüş. Araştırıp bakmış ki böbrek yok. Kurda dönüp:}

@p{— Devenin böbreği yok, nereye gitti? demiş. Böyle deyince, kurt tilkiye
dönüp, bakmış.}

@p{Tilki:}

@p{— Niye bana bakıyorsun ki? Böbreği yiyip, bitirdin, şimdi de bana dönüp,
bakıyorsun. Ben sana yeme dedim, demiş. Böyle demesiyle, ayı kurda kızıp, kurdu
kovalayarak, gitmiş. Ayının gitmesiyle, tilki devenin etini kamışlığa taşıyıp,
saklamış. Ayı bir süre dolaşıp, yorgun argın geri dönmüş. Ayı döndüğünde tilki,
deveyi kestikleri yerde ağlayıp duruyormuş. Ayı:}

@p{— Devenin eti nerede? demiş.}

@p{Tilki:}

@p{— Ayı ağabey, devenin etini kartal götürdü, ben de buna üzülüp, ağlıyorum,
demiş.}

@p{Ayı çok kızıp: “Bunlar ile yaşanmaz!” diyerek, kendi yoluna gitmiş. Fakat
hilekar tilki sakladığı deve etini yiyip, öylece günlerini gün etmiş.}
