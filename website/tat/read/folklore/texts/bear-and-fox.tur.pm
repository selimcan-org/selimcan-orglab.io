#lang pollen

@h3{Ayı ile tilki [2]}

@p{Çok eski zamanlarda bir ayı ile bir tilki arkadaş olmuşlar. Bunlar uzun süre
arkadaş olarak yaşamışlar. Günleri böyle, geçerken ayı yaşlanmış ve yürüyemez
olmuş.}

@p{Ayı ile tilkinin arkadaş olarak yaşadıkları vakitlerde birlikte topladıkları
bir fıçı balları, bir fıçı tereyağları varmış. Bunlar, bal ile yağı çok uzak
bir yere saklamışlar ve yaşlılık günlerinde birlikte yeriz diye, sözleşmişler.}

@p{Günlerden bir gün tilki sessizce çıkıp, gitmiş. Erkenden çıkıp giden tilki,
akşam olunca geri gelmiş. Tilki dönüp, gelince ayı:}

@p{— Sen nereye gittin, tilki kardeş? demiş.}

@p{— Ben filan köye gittim, filan kişinin karısı bir çocuk doğurmuş, ben onun
çocuğuna isim koymaya gittim, demiş.}

@p{— Erkek mi, kız mı? demiş.}

@p{— Erkek, demiş.}

@p{— Adını ne koydunuz? demiş.}

@p{— “Başlamış” koydum, demiş tilki.}

@p{— Peki, sen bana hediye filan alıp gelmedin mi? demiş.}

@p{— Sana verecek bir şeyleri yoktu, yiyecek içecekleri de çok değildi, demiş
tilki.}

@p{— Şimdi senin karnın tok ne de olsa, tilki kardeş, fakat ben nasıl karnımı
doyuracağım? demiş ayı.}

@p{Tilki ayıya oradan buradan toplanan çeşitli artıkları getirip vermiş. Ayı, ne
yapsın, bu ufak tefek şeylerle avunmuş.}

@p{Günlerden bir gün tilki yine çok erken kalkmış ve sessizce çıkmış gitmiş. Bu
sefer de bal ve yağ fıçılarının yanına gidip, doyana kadar yemiş, dönmüş. O gün
ayı, tilkiyi daha da yorgun bir şekilde bekleyip: “Yine nereye kayboldu bu,
yoksa bir yerde yemekte filan mı acaba?” demiş. Gece olup, karanlık çökerken
tilki dönüp, gelmiş ve:}

@p{— Nasılsın, ayı kardeş? İyi misin? demiş.}

@p{— Eh işte. Sen nerelere gidip, geldin? diye sormuş ayı.}

@p{— Yemeğe gittim, demiş.}

@p{— Kime, hangi kişiye? demiş.}

@p{— Filan kişinin karısı bir çocuk doğurmuş, onların çocuklarına isim koymaya
gittim, demiş.}

@p{— Hay hay, beni hiç götürmüyorsun sen, demiş ayı.}


@p{— Seni götürmek için uygun değil, ayı kardeş, onlar sadece beni büyük bir
hürmetle çağırdılar, demiş.}

@p{— Ziyafetleri güzel miydi?}

@p{— Çok güzel, çok boldu.}

@p{— Erkek çocuk mu, kız çocuğu mu?}

@p{— Erkek çocuk.}

@p{— Adını ne koydunuz?.}

@p{— Adını “Yarılanmış” koydum.}

@p{— Hay hay, çok güzelmiş. Yalnız, sen bana hediye de alıp, getirmemişsin.}

@p{— Sana hediye alıp, gelmek mümkün olmadı, ayı kardeş, onların çocukları da
çoktu.}

@p{Böyle demiş tilki ve tekrar yatıp uyumuş. Dinlenip, rahatladıktan sonra o
civarda dolaşarak, ayıya ufak tefek artık şeyler alıp, getirmiş. Zavallı ayı,
ne yapsın, kendisi çıkıp yürüyemeyince, tilkinin getirdiklerine razı olmuş.}

@p{Bir hafta, on gün geçince, tilki yine çok erken çıkıp, gitmiş. Erken gitmiş,
geç dönmüş. Tilki dönüp, gelince, ayı:}

@p{— Nereye gittin? diye sormuş.}

@p{— Yemeğe gittim, demiş tilki.}

@p{— Kime?}

@p{— Köyün öbür ucundaki filan kişinin karısı çocuk doğurmuş, o çocuğa isim
koymaya çağırmışlardı, oraya gittim.}

@p{— Erkek mi, kız mı?}

@p{— Erkek.}

@p{— Ay, hay hay, her zaman erkek çocuklara isim koymaya gidiyorsun. Adını ne
koydunuz?  demiş.}

@p{— “Diplenmiş” koydum, demiş tilki.}

@p{— Peki bana bir şey getirmedin mi?}


@p{— İşte azıcık hediye alıp geldim, “Pek çok olmadı, ayıplamasın şimdi”
dediler.}

@p{Tilki bayat ekmek parçasına çok ince bal ile yağ bulaştırıp, getirmiş.}

@p{— Sağ olsunlar, beni aklına getiren yoktu, çocukları bahtlı olsun,
diye diye yemiş.}

@p{Tilki ertesi sabah yine çıkıp gitmiş, bir yerlerde dolaşıp, akşam olunca
yine dönmüş, karnı tok, kaygısı yok bunun.}

@p{Ayının dayanacak hâli kalmamış. Bir gün böyle tilkinin dönmesini beklemiş ve
tilki gelince:}

@p{— Bak hele, arkadaş, demiş, bizim kara gün için sakladığımız bal ve yağlara gidip,
bakmak lazım, sonunda ben açlıktan ölmekten korkmaya başladım, açın halini tok
bilmiyor, sen benim halimi bilmiyorsun.}

@p{— Tamam, gideriz, demiş tilki.}

@p{Erkenden kalkıp gitmişler bunlar, bal ve yağ sakladıkları yere. Gidip,
ulaşmışlar. Fıçıları sakladıkları yerden çıkarıp bakmışlar ki fıçılar boş; bal
ile yağın yerinde yeller esiyor. Ayı çok sinirlenerek, tilkiye:}

@p{— Ey arkadaş, sen çıkıp çıkıp, kayboluyordun; bunları sen yiyip
bitirmişsindir, demiş.}

@p{— Hayır, ben yemedim, demiş tilki; gözünü de kırpmadan ayıyı aldatmaya
çalışmış.}

@p{Ayı inanmamış:}

@p{— Senin işin bu, demiş.}

@p{Tilki:}

@p{— Ben yemeğe, su içmeye gittiğim vakitlerde, buraya gelip bal ve yağı sen
kendin yiyip, bitirmişsindir, demiş. Haydi, ikimiz de karınlarımızı güneşe
koyup, yatalım; bal ve yağı kim yemişse onun karnından eriyip çıkar, demiş.}

@p{Ayı ile tilki karınlarını güneşe dönüp, yatmışlar. Yattıktan sonra, ayı
güneşin sıcağında rahatlayıp uykuya dalmış. Ayının uykuya dalması ile tilki
kalkıp, fıçıların dibinde kalan bal ve yağı kazımış ve ayının karnına
sıvamış. Bundan sonra tilki, ayıyı uyandırıp:}


@p{— Kalk kalk, ayı kardeş, ben yemeye ve içmeye gittiğim vakitlerde bal ile
yağı sen yiyip bitirmişsindir, demiştim, doğruymuş, bal ile yağı gerçekten de
sen yiyip, bitirmişsin, demiş.}

@p{— Hayır, tilki kardeş, bal ile yağı ben yemedim, demiş.}

@p{— Sen yemediysen, kim yedi öyleyse, demiş. Bu, bal ile yağın nerede
saklandığını bilen birinin işi, demiş.}

@p{— Eh, tilki kardeş, sen ne de olsa benim hasta olduğumu, çıkıp
yürüyemediğimi biliyorsun, demiş.}

@p{— Konuşup durma şimdi, ayı kardeş, işte çalıp, yediğin yağlar eriyip
karnının dışına çıktı şimdi, demiş.}

@p{Böyle demiş ve bu hilekar tilki kendi hasta arkadaşını terk edip, gitmiş.}
