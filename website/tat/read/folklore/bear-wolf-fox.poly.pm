#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@(require (rename-in (only-in "texts/bear-wolf-fox.tat.pm" doc)
           (doc TAT)))
@(require (rename-in (only-in "texts/bear-wolf-fox.tur.pm" doc)
           (doc TUR)))

@(define (empty-line? l)
  (and (string? l) (regexp-match #rx"\\s*\n\\s*" l)))

@(for/splice ([tat (filter (λ (e) (not (empty-line? e))) (cdr TAT))]
              [tur (filter (λ (e) (not (empty-line? e))) (cdr TUR))])
 @div[#:class "page"]{
  @div[#:class "lefthalf"]{@tat}
  @div[#:class "righthalf"]{@tur}})

@h3{Чыганаклар / Kaynaklar}

@ol{

@li{Гатина, Ярми (1977) Халык авыз иҗаты. Беренче том. Әкиятләр}

@li{Mustafa Gültekin (2010) Tataristan masallari üzerinde bir araştırma. T.C.
Ege üniversitesi sosyal bilimler enstitüsü Türk Halk Bilimi Anabilim dalı
(doktora tezi)}

}
