#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@(require (rename-in (only-in "texts/sick-carrying-healthy.tat.pm" doc)
           (doc SICK-CARRYING-HEALTHY-TAT)))
@(require (rename-in (only-in "texts/sick-carrying-healthy.tur.pm" doc)
           (doc SICK-CARRYING-HEALTHY-TUR)))

@(for/splice ([tat (cdr SICK-CARRYING-HEALTHY-TAT)]
              [tur (cdr SICK-CARRYING-HEALTHY-TUR)])
 @div[#:class "page"]{
  @div[#:class "lefthalf"]{@tat}
  @div[#:class "righthalf"]{@tur}})

@h3{Чыганаклар / Kaynaklar}

@ol{

@li{Гатина, Ярми (1977) Халык авыз иҗаты. Беренче том. Әкиятләр}

@li{Mustafa Gültekin (2010) Tataristan masallari üzerinde bir araştırma. T.C.
Ege üniversitesi sosyal bilimler enstitüsü Türk Halk Bilimi Anabilim dalı
(doktora tezi)}

}
