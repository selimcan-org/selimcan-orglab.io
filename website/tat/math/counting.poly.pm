#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Counting} @tat{Санау}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/6SF09CROAVI"
         #:english "https://www.youtube-nocookie.com/embed/y2-uaPiyoxc"
         #:tatar "https://www.youtube-nocookie.com/embed/y2-uaPiyoxc"]{@video-title{@u{@eng{Counting with small numbers} @tat{Кечкенә саннар белән санау}}}}

   @practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-counting/e/counting-out-1-20-objects"
                #:target "_blank"]{@practice-title{@u{@eng{Practice: Count with small numbers} @tat{Күнегү: кечкенә саннар белән сана}}}}}
                                                                       
   @kavideo[#:frank "https://www.youtube-nocookie.com/embed/XrC8pqBXCK4"
          #:english "https://www.youtube-nocookie.com/embed/PEeUTQ0Gri8"
          #:tatar "https://www.youtube-nocookie.com/embed/PEeUTQ0Gri8"]{
     @video-title{@u{@eng{Counting in order} @tat{Рәттән дөрес санау}}}}

   @practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-counting/e/counting-objects"
                #:target "_blank"]{@practice-title{@u{@eng{Practice: Count in order} @tat{Күнегү: рәттән дөрес санау}}}}}

   @practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-counting/e/one-more--one-less"
                #:target "_blank"]{@h5{@practice-title{@eng{Practice: Find 1 more or 1 less than a number} @tat{Күнегү: бер саннан 1-гә зуррак я 1-гә кечерәк (санны) табу}}}}}
