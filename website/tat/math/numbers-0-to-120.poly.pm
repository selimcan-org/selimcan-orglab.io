#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Numbers 0 to 120} @tat{0-дән алып 120-гә кадәрге саннар}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-numbers-120/e/count-from-any-number" #:target "_blank"]{@practice-title{@u{@eng{Practice: Missing numbers} @tat{Күнегү: төшеп калган саннар}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/2VpsRGKRo7o"
       #:english "https://www.youtube-nocookie.com/embed/9XZypM2Z3Ro"
       #:tatar "https://www.youtube-nocookie.com/embed/9XZypM2Z3Ro"]{@video-title{@u{@eng{Number grid} @tat{Саннар җәдвәле}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/tS3i1w8rLjY"
       #:english "https://www.youtube-nocookie.com/embed/1AqkBdCBm9o"
       #:tatar "https://www.youtube-nocookie.com/embed/1AqkBdCBm9o"]{@video-title{@u{@eng{Missing numbers between 0 and 120} @tat{0 белән 120 арасында төшеп калган саннар}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-numbers-120/e/count-to-100"
                #:target "_blank"]{@practice-title{@u{@eng{Practice: Numbers to 100} @tat{Күнегү: 100-гә кадәрге саннар}}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-numbers-120/e/numbers-to-120"
                #:target "_blank"]{@practice-title{@u{@eng{Practice: Numbers to 120} @tat{Күнегү: 120-гә кадәрге саннар}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/c0OQ3r4P8PQ"
       #:english "https://www.youtube-nocookie.com/embed/UnPpFw3natI"
       #:tatar
       "https://www.youtube-nocookie.com/embed/UnPpFw3natI"]{@video-title{@u{@eng{Counting
       by tens} @tat{Унарлап санау}}}}
	     	     
@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-numbers-120/e/counting-tens"
                #:target "_blank"]{@practice-title{@u{@eng{Practice: Count tens} @tat{Күнегү: дистәләрне сана}}}}}
