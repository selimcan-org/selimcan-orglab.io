#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Put together, take apart} @tat{Тупла, тарат}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/GUkQCoJjkDw"
         #:english "https://www.youtube-nocookie.com/embed/ie0waMJxnTs"
         #:tatar "https://www.youtube-nocookie.com/embed/ie0waMJxnTs"]{@video-title{@u{@eng{Add and subtract: pieces of fruit} @tat{Куш һәм ал: җиләк-җимеш данәләре}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/-94IDWZ2CaI"
         #:english "https://www.youtube-nocookie.com/embed/kpEJwpemL2Q"
         #:tatar "https://www.youtube-nocookie.com/embed/kpEJwpemL2Q"]{@video-title{@u{@eng{Addition and subtraction within 10} @tat{10-га кадәр[ге саннарны] кушу һәм алу}}}}
         
@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-together-apart/e/put-together"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Add within 10} @tat{Күнегү: 10-га кадәр[ге саннарны] куш}}}}}
            
@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-together-apart/e/take-apart"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Subtract within 10} @tat{Күнегү: 10-га кадәр[ге саннарны] ал}}}}}
             
