#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Relate addition and subtraction} @tat{Кушу һәм алуны бәйлә}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/s6fl76-XnrI"
         #:english "https://www.youtube-nocookie.com/embed/zVLjWIftX_o"
         #:tatar "https://www.youtube-nocookie.com/embed/zVLjWIftX_o"]{@video-title{@u{@eng{Relating addition and subtraction} @tat{Кушу һәм алуны бәйләү}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-add-subtract-10/e/relate-addition-and-subtraction"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Relate addition and subtraction} @tat{Күнегү: кушу һәм алуны бәйлә}}}}}
