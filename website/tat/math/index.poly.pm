#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "KhanAcademy . org / math Tatarça with a-la Frank subtitles (unofficial)")
@(define-meta author           "")

@(define LANG "tt")
@(define HEIGHT "480")

@p{Бу биттә математикага өйрәтүче, @a[#:href "https://www.khanacademy.org" #:target "_blank"]{Хан Академиясе} әзерләгән видеолар һәм күнегүләр@numbered-note{Күнегүләргә сылтамалар гына куячакбыз.} тупланган.}

@p{Һәр видео өч төрле форматта биреләчәк: 1) @a[#:href "http://www.franklang.ru/index.php/metod-chteniya-ili-franka" #:target "_blank"]{Илья  Франк ысулы} буенча адатацияләнгән текстларга охшаган субтитрлар белән, 2) инглизчә субтитрлар белән, һәм 3) татарча субтитлар белән.@numbered-note{Беренче һәм өченче төр субтитрларны без (ягъни selimcan.org) әзерләдек.}
 Мәсәлән, менә болай: @kavideo[#:frank "https://www.youtube-nocookie.com/embed/6SF09CROAVI"
         #:english "https://www.youtube-nocookie.com/embed/y2-uaPiyoxc"
         #:tatar "https://www.youtube-nocookie.com/embed/y2-uaPiyoxc"]{@video-title{@u{@eng{Counting with small numbers} @tat{Кечкенә саннар белән санау}}}}}

@p{Өч төрле cубтитрларның булуы Сезгә (балагызга) бер үк вакытта инглизчәне дә, математиканы да өйрәнергә ярдәм итәр дигән өметтә калабыз.}

@p{Инглизчәне өйрәнә генә башлаган булсагыз, без Сезгә ул видеоларны китерелгән тәртиптә карарга киңәш итәбез (ягъни башта "Франкча" икетелле субтитрлар белән,@numbered-note{Мондый төр видеоларны караганда видеоны еш-еш туктатырга туры киләчәктер, чөнки субтитрлар бер-берсен бик тиз алыштыра. Моның өчен видеоның теләсә кайсы җиренә басу да җитә, нәкъ «пауза» төймәсенә генә басу мәҗбүри түгел.} аннан инглизчә субтитрлар белән, һәм, теләсәгез, татарча субтитрлар белән). Беренче кат караганда субтитрларны сүндереп карау да файдалы --- инглиз сөйләмен аңларга тизрәк өйрәнәчәксез.}

@p{Видеоны тулы экранда карау өчен, видеоның аскы уң почмагындагы шакмакка басыгыз яки видеога ике тапкыр «чиртегез».}

@section{Күнегүләр}

@p{Видеолардан тыш, selimcan.org'та Хан Академиясе күнегүләренә сылтамалар күрерсез. Мәсәлән:}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-counting/e/counting-out-1-20-objects"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Count with small numbers} @tat{Күнегү: кечкенә саннар белән сана}}}}}

@p{Бер темага кагылышлы видео һәм күнегү җыелмасын без "дәрес" дип атыйбыз.@numbered-note{@strong{Озын сүзнең кыскасы: әзер дәресләр исемлеген бу битнең иң аскы өлешендә күреп була @hyperlink["#lessons"]{⇣}}}
}

@p{Күнегүләрнең әлегә берсе дә татар телендә юк, әмма тора-бара аларны да тәрҗемә итәрбез дип торабыз, чөнки ул күнегүләр --- Хан Академиясенең иң көчле якларының берсе. Алар артында кызыклы алгоритмнар тора, ул алгоритмнар ярдәмендә күнегүләр һәр укучыга яраклаша һәм ул укучы өчен виртуль бер репетитор ролен уйный, ягъни мәсәлән.@numbered-note{Мәңге бушлай репетитор}}

@section{Хан Академиясе материалларын татарчага тәрҗемә итү турында}

@p{Хан Академиясе сайты татарча да эшли башласын өчен (һәм күнегүләрне тәрҗемә итәргә мөмкинлек пәйда булсын өчен), иң элек @a[#:href "https://www.khanacademy.org/math" #:target "_blank"]{khanacademy.org/math}’ның кимендә бер кисәгендәге (Early Math, Arithmetic, Albegra 1, Algebra 2 һ.б.) барлык видеолар да тәрҗемә ителгән булырга тиеш.@numbered-note{Күбрәк мәгълүматны
  @a[#:href "https://khanacademy.zendesk.com/hc/en-us/articles/226412428-What-are-our-goals-and-milestones-" #:target "_blank"]{монда} табып була.}}

@p{Без эшне «Early Math» өлешендәге видеоларны тәрҗемә итүдән башладык, чөнки бу иң кечкенә кисәк.}

@p{Субтитрларны тәрҗемә итү Хан Академиясенең @a[#:href "https://www.youtube.com/channel/UCs8a-pNM8EHKKU28XQLetLw" #:target "_blank"]{рәсми Youtube каналында} башкарыла. Инглизчә дә, татарча да яхшы беләсез һәм субтитрларны тәрҗемә итәргә ярдәм итәсегез килә икән, күңелегезгә хуш килгән һәм татарча субтитрлары булмаган видеоның аскы уң чатындагы @img[#:src "icons/gear.svg" #:alt "сигезпочмак" #:height "15"] «Settings» билгесенә, һәм аннан соң ачылган менюда «Subtitles → Add subtitles» төймәләренә басыгыз.}

@p{Тулысынча (ягъни тавышы да, видеодагы язулар да) татар теленә тәрҗемә ителгән видеоларны @a[#:href "https://www.youtube.com/channel/UCvFdUw6YYN2cD5HHwt_jJSA" #:target "_blank"]{KhanAcademyTatarchaUnofficial} YouTube каналында карап була.}

@p{@a[#:href "https://www.youtube.com/channel/UC606rHdIrgR_qLGvPYgO1Xw" #:target "_blank"]{Selimcan.org/Extra} YouTube каналында исә өстәге беренче төр видео шикелле, икетелле субтитрлы видеолар тупланган.}

@h2[#:id "lessons"]{Дәресләр}

@ol{
  @li{@smallcaps{@hyperlink["counting.html"]{@u{@eng{Counting} @tat{Санау}}}}}
  @li{@smallcaps{@hyperlink["numbers-0-to-120.html"]{@u{@eng{Numbers 0 to 120} @tat{0-дән алып 120-гә кадәрге саннар}}}}}
  @li{@smallcaps{@hyperlink["counting-objects.html"]{@u{@eng{Counting objects} @tat{Затларны санау}}}}}
  @li{@smallcaps{@hyperlink["comparing-small-numbers.html"]{@u{@eng{Comparing small numbers} @tat{Кечкенә саннарны чагыштыру}}}}}
  @li{@smallcaps{@hyperlink["what-is-addition-what-is-subtraction.html"]{@u{@eng{What is addition? What is subtraction?} @tat{Кушу нидер? Алу нидер?}}}}}
  @li{@smallcaps{@hyperlink["making-small-numbers.html"]{@u{@eng{Making small numbers} @tat{Кечкенә саннар хасил итү}}}}}
  @li{@smallcaps{@hyperlink["making-10.html"]{@u{@eng{Making 10} @tat{10-ны хасил итү}}}}}
  @li{@smallcaps{@hyperlink["put-together-take-apart.html"]{@u{@eng{Put together, take apart} @tat{Тупла, тарат}}}}}
  @li{@smallcaps{@hyperlink["relate-addition-and-subtraction.html"]{@u{@eng{Relate addition and subtraction} @tat{Кушу һәм алуны бәйлә}}}}}
  @li{@smallcaps{@hyperlink["addition-and-subtraction-word-problems.html"]{@u{@eng{Addition and subtraction word problems} @tat{Кушу һәм алу[га] сүз [белән бирелгән] мәсьәләләр}}}}}
  @li{@smallcaps{@hyperlink["teens.html"]{@u{@eng{Teens} @tat{Уннан алып унтугызга кадәрге саннар}}}}}
  @li{@smallcaps{@hyperlink["tens.html"]{@u{@eng{Tens} @tat{Дистәләр}}}}}
}

@p{Бер дәрестән икенчесенә күчү өчен һәр битнең өске сул чатындагы "Алга →" төймәсенә басыгыз.}
