#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{What is addition? What is subtraction?} @tat{Кушу нидер? Алу нидер?}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/CCmr39u43lw"
         #:english "https://www.youtube-nocookie.com/embed/fsTD_jqseBA"
         #:tatar "https://www.youtube-nocookie.com/embed/fsTD_jqseBA"]{@video-title{@u{@eng{Intro to addition} @tat{Кушуга кереш}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-add-sub-intro/e/addition_1"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Add within 5} @tat{Күнегү: 5-кә кадәр[ге саннарны] кушу}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/YaTbpU03sk0"
         #:english "https://www.youtube-nocookie.com/embed/AO9bHbUdg-M"
         #:tatar "https://www.youtube-nocookie.com/embed/AO9bHbUdg-M"]{@video-title{@u{@eng{Intro to subtraction} @tat{Алуга кереш}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-add-sub-basics/cc-early-math-add-sub-intro/e/subtraction_1"
             #:target "_blank"]{@practice-title{@u{@eng{Practice: Subtract within 5} @tat{Күнегү: 5-кә кадәр[ге саннарны] алу}}}}}
