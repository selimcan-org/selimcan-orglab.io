#lang pollen

@(define-meta title "Selimcan.org уку-укыту программасы турында бирелә торган
кайбер сораулар һәм программаны куллану тәртибе")
@(define-meta doc-publish-date "2019-01-16")
@(define-meta author "")
@(define-meta series "")
@(define-meta summary "")

@p{@strong{Игътибар: әлегә бу бер каралама гына.}}

@section{Selimcan.org программасы буенча уку нидән гыйбарәт?}

@p{Һәр атнаның биш я алты көнендә укучы:}

@ol{

@li{эшен математикадан башлый, аерым алганда: X минут
@hyperlink["https://www.khanacademy.org/mission/math"]{
https://www.khanacademy.org/mission/math} сәхифәсендә, үткәнне ныгытыр өчен,
мәсьәләләр чишә@numbered-note{@hyperlink["https://www.khanacademy.org/mission/math"]{
https://www.khanacademy.org/mission/math} бите һәркем өчен дә индивидуаль булып,
аның алга китешен теркәп барачак, шуңа күрә башта "Create an account", һәм аннан
соң "Learner" төймәсенә басып, теркәлү кирәк.}}

@li{X минут @hyperlink["https://www.khanacademy.org/math"]{
https://www.khanacademy.org/math}@numbered-note{khanacademy.org сайты
татарча эшли башлаганчы,
@hyperlink["http://selimcan.org/tat/math/"]{http://selimcan.org/tat/math/}
сәхифәсен кулланырга мөмкин} сәхифәсендә яңа тема өйрәнә һәм яңа темага бирелгән
күнегүләрне үти}

@li{X минут бер битлек эссе/инша,
(@hyperlink["https://tt.wikipedia.org"]{Википедия}) мәкаләсе яки хат
яза яки башка кеше язганны тәрҗемә итә}

@li{2*X минут без төзегән исемлектән алынган чираттагы (исемлекнең тәртибен
бозмыйча ягъни!) китап укый һәм ул китапка кагылышлы сорауларга җавап бирергә
тырыша.}}

@p{15 минут < X < 60 минут.}

@p{Берничә атна саен укучы үткән темалары һәм укыган китаплары буенча зуррак язма
имтихан бирә.}

@p{Сәгатьләр буе селкенмичә өстәл артында утыру беркем өчен дә файдалы түгел,
бигрәк тә сабый балалар өчен, шуңа һәр ярты сәгать саен укучы кыска гына тәнәфес
ясап, торып, язылып ала.@numbered-note{Иң кулае ---
@hyperlink["https://en.wikipedia.org/wiki/Pomodoro_Technique"]{"помидор ысулын"}
кулланып, 25 минут игътибар белән укыганнан соң, 5 минут ял итү.}
}

@p{Шул рәвешле укучы, төп игътибарын мәсьәлә чишү сәләтен үстерүгә юнәлтеп,
арифметикадан алып математик анализга кадәр (башкача исеме: калькулюс)
математиканы өйрәнә; һәм, аңа бирелгән сораулар ярдәмендә үз белемен тикшереп,
программада булган китаплардагы белемне үзләштерә һәм сүз байлыгын арттыра.}

@p{@hyperlink["https://www.khanacademy.org/math"]{https://www.khanacademy.org/math}-та
өйрәнәсе бүлекләр менә болар: Early math -> Arithmetic -> Basic geometry ->
Pre-algebra -> Algebra basics -> Algebra I -> High school geometry -> Algebra II
-> Trigonometry -> Statistics and probability -> High school statistics ->
Precalculus -> Calculus I -> Calculus II.}

@p{Алгебрага җитеп, функцияләр белән танышкач, факультатив рәвешендә (әмма укылуы
үтә дә хупланылган!) компьютер программаларын инша итүнең төп принципларын
өйрәтүче @hyperlink["https://htdp.org"]{"How to Design Programs"} китабын укый
башларга мөмкин.}

@section{Программаның үзенчәлекләре нинди?}

@p{Материалны сыйныфларга бүлү юк.}

@p{Программа көнлек@numbered-note{Атнасына бер яки ике көн ялны; елына бер-ике ай
каникулны санамаганда, әлбәттә.} "математика (мәсьәлә чишү)--язу--уку"
сессияләреннән тора. Уку сессияләрен география, әдәбият, тарих, икътисад
һ.б.ш. фәннәргә бүлү дә юк. Моның урынына --- шушы өлкәләрдән белем бирүче,
эчтәлеге буенча да, теле буенча да җиңелдән кыенга таба, логик эзлеклелектә
тезелгән китаплар уку һәм ул китапларга бәйле сораулар, биремнәргә җавап бирү.}

@p{Бу китаплар арасында бүгенге мәктәпләрдә өйрәнелгән физика, химия һәм биология
дәреслекләре юк. Калькулюсны үзләштергәч кенә@numbered-note{укы: чыгарылма һәм
интеграл ни икәнен белгәндә һәм аларны озак уйлап тормыйча куллана алганда},
укучы гадәттә университетның беренче курсында укыла торган физика, химия һәм
биология@numbered-note{бер-бер артлы, нәкъ шул тәртиптә} дәреслекләре буенча
шөгыльләнә. Калькулюс тәмамлаганчы ук бала бу фәннәргә бәйле нәрсәләр белән
кызыксына икән (әйтик, бөҗәк, күбәләк төрләре белән, яки сирәк ташлар җыю
белән, я радиотехника белән я башкалар), балага ул әйберләр турында сөйләүче
фәнни-популяр китаплар алып бирү яхшы, әлбәттә, әмма ул китаплар программадан
тыш бер хобби санала.}

@p{Тарихи вакыйгаларны бу вакыйгалар турында сөйләүче хәзерге заман китапларыннан
(гына) түгел, ә ул вакыйгаларда катнашкан шәхесләрнең биографияләрен һәм
автобиографияләрен укып өйрәнүгә зур урын бирелә.}

@section{Программаның максатлары нинди?}

@ol{

@li{@strong{Традицион, кәгазь, беренче карашка бер дә кызыклы булмаган,
иллюстрацияләргә ярлы китаплардан да мөстәкыйль рәвештә белем ала белергә
өйрәнеп һәм бу эшне гадәткә кертеп, укучыга SAT, AP һәм ЕГЭ/БДИ имтиханнарына
әзерләнергә булышу;}}

@li{@strong{үз туган телендәге мәдәни мирас һәм берникадәр фәнни
терминология белән таныштыру;}}

@li{@strong{көндәлек язу (һәм инде язылганны "шомарту") күнегүләре аша, укучыда язуга
осталык һәм, нәтиҗәдә, язуга мәхәббәт тәрбияләү}}

}

@p{--- selimcan.org программасының төп максатлары
шулар.}

@p{Мөстәкыйль рәвештә белем алырга өйрәнү мөһим, чөнки мәктәптә чагында баланы
"кулыннан җитәкләп" укытып булса да, мәктәпне тәмамлагач мондый ярдәм
булмаячак. Мөстәкыйль рәвештә китаплардан белем ала белү сәләте һәм моңа
кирәкле гадәтләр яки дисциплина исә университетта укыганда да, күпчелек заманча
эш урыннарында да нәтиҗәле булуның төп шарты.}

@section{Selimcan.org уку-укыту программасының нинди фазыйләтләре бар?}

@p{Гади, ягъни педагогик белеме дә, күп вакыты да булмаган ата-ана гамәлгә
ашырырлык. Ата-ананың эше укучы язган иншаларны укып чыгып, хаталарның астына
чыгып сызудан һәм укучының укуда алга бару-бармавына күз-колак булып торудан
гыйбарәт.}

@p{Балаларны мөстәкыйль рәвештә белем алырга өйрәтә. Технологияләр һәм һөнәрләр
торган саен тизрәк үзгәреп торучы дөньяда бу иң мөһим һөнәр!}

@p{Күп чыгымнар сорамый --- Хан Академиясе материаллары бушлай һәм мәңге бушлай
булып калачак; без сайлаган китапларның да pdf версияләре бушлай
булачак. Pdf-ларны йөкләп алып, принтер аша бастырып, алардан үзегез китап ясый
алачаксыз.}

@p{Укучы белемне мөстәкыйль рәвештә, китаплардан алырга өйрәнгәнгә күрә, ул
өйрәнергә мөмкин белем әти-әнисендә булган белем белән чикләнми.}

@section{Программа нәтиҗәле булсын өчен, укучыга һәм укытучыга таләпләр нинди?}

@subsection{Укучыга булган таләпләр}

@p{Selimcan.org программасы мөстәкыйль рәвештә китап укуга нигезләнә. Хәреф тану
һәм укуны бала өлкәннәр ярдәменнән башка өйрәнә алмый, әлбәттә. Шуңа күрә, бу
программа буенча шөгыльләнә башлый алыр өчен, бала хәреф таный һәм аерым
сүзләрне укый белергә тиеш.}

@p{Хан Академиясенең башлангыч математика өлешендә санарга өйрәтүче
күнегүләр җитәрлек түгел дип саныйбыз. Анда санауга күнегүләр бирелә, әмма, ул
күнегүләргә керешкәнче, бала инде 100-гә кадәр саный белергә тиеш
безнеңчә.}

@p{Соңрак бу сайтка балаларны ничек өйдә, җиңел генә, укырга һәм санарга өйрәтеп
була икәне турында сөйләүче материаллар өстәләчәк.}

@p{Мөмкинлегегез булса, уку, язу-сызулар өчен аерам бер бүлмә булдырып, ул бүлмәдә
һәр бала өчен уңайлы урындыклы аерым бер язу өстәле урнаштырыгыз. Уку вакытында
ул бүлмәдә балаларга беркем дә комачауламаска тиеш, әлбәттә.}

@subsection{Укытучыга/ата-анага булган таләпләр}

@p{@strong{Selimcan.org программасы эшләсен өчен төп таләп --- нәкъ бала үз язу
өстәле артында укуга сарыф иткән кадәр вакытны Сез дә үз язу өстәлегез артында
булып, акыл хезмәте белән шөгыльләнергә тиешсез.} Бу бигрәк тә бала кечкенә
чакта мөһим.}

@p{Балалар ата-анасыннан күреп өйрәнәләр. Әгәр дә Сез китап та, гәҗит-журнал да
укымыйсыз икән, балагыздан моны көтү я таләп итү --- мәгънәсез эш.}

@subsection{Ниләр эшләргә ярамый?}

@p{Мәсьәләләрне бала өчен чишеп бирү. Ул аларны үзе чишәргә өйрәнергә тиеш. Хан
Академиясендә, мәсьәләне чишә алмасаң, программа бу темага булган видеоларны
янәдән караргә тәкъдим итә. Бу яхшы адым. Мәсьәләнең чишелешен башка кешедән
алу исә яхшы адым түгел. Болай эшләсәгез, балагыз мәсьәләне үзең чишү
нәтиҗәсендә килә торган өйрәнүдән, мәсьәләне чишә алганын күреп үз көченә
күбрәк ышанудан һәм шатланудан мәхрүм калачак.}

@section{Аңлашылды. Программа үзе, ягъни китаплар кайда соң? Күрәсе килә.}

@p{Чынлыкта программаның беренче версиясе дүрт кисәктән торачак:}

@ol{

@li{Хан Академиясенең математика өлеше}

@li{татар телендәге китаплар}

@li{рус телендәге китаплар}

@li{инглиз телендәге китаплар}}

@p{Инглиз телендәге китапларны сайлаганда, нигез итеп Dr. Robinson'ның
@hyperlink["https://www.robinsoncurriculum.com/rc/homeschool-curriculum-excellence/"]{
уку программасын} алырга булдык.@margin-note{Робинсон әфәнденең дәүләт мәктәпләренә булган кискен тәнкыйди
карашы белән бер дә килешмәсәк тә, ул төзегән уку-укыту программасы, статистик
мәгълүматларга күрә, үтә дә нәтиҗәле булырга охшый.}
}

@p{Әле без Хан Академиясенең "Early Math" ("Башлангыч математика") өлешендәге
видеоларны татар теленә тәрҗемә итү белән мәшгуль.}
 
@section{Программа тулысынча әзер булсын өчен нәрсәләр эшлисе бар?}

@p{Төп эшләнәсе эшләр алар менә болар:}

@ol{

@li{Хан Академиясенең математика өлешен татар теленә тәрҗемә итү. Болар
видеолар, текстлар, күнегүләр.}

@li{Робинсон программасындагы китапларны укып@numbered-note{Тулы исемлек
@hyperlink["http://users.gobigwest.com/rosegate/RCbooks.html"]{менә монда}
бар}, алар эчтәлеге ягыннан безнең программага туры киләме-юкмы икәнен тикшереп
чыгу. Туры-килмәсә, аларга бушлай алмаш китаплар табу. Сайлап алынган
китапларга @hyperlink["../extra/frank/"]{selimcan.org/extra/frank/} битендәгечә,
инглиз телен өйрәнергә ярдәм итүче аннотацияләр өстәп, үзебезнең сайтка кую.}

@li{Татарча китаплар сайлау һәм әзерләү. Сайлау дигәндә, сүз татар әдәбиятынан
һәм тарихыннан булган әсәрләрне укып, һәркем тарафыннан да укылырга тиеш дип
саналганнарын исемлеккә кертү турында бара. Моннан тыш, география, икътисад һәм
башка фәннәрнә өйрәтүче китаплар кирәк булачак. Аларын Робинсон программасыннан
алып, шуларны тәрҗемә итәрбез дип торабыз.}

@li{Русча китаплар сайлау һәм әзерләү. Сайлау дигәндә, сүз рус әдәбиятынан һәм
тарихыннан булган әсәрләрне укып, һәркем тарафыннан да укылырга тиеш дип
саналганнарын исемлеккә кертү турында бара. Моннан тыш, география, икътисад һәм
башка фәннәрнә өйрәтүче китаплар кирәк булачак. Аларын Робинсон
программасыннанмы, башка җирдәнме алып, шуларны (кирәк булса) тәрҗемә итәрбез
дип торабыз.}

}

@section{Без нигә яңа уку-укыту программасын төзергә булдык?}

@p{Әлеге уку-укыту программасы дүрт төркем кешене күз уңында тотып төзелә:}

@ol{

@li{үз ватаннарыннан читтә яшәп тә, балаларының туган телләрендә дә
укырга-язарга өйрәнүен һәм белем алуын теләүче (әмма янәшәләрендә ул телдә
белем бирүче мәктәп булмаган) гаиләләр;}

@li{мәктәп программасы белән генә канәгать булмаган, кызыксынучан укучылар;
мөстәкыйль рәвештә шөгыльләнсеннәр дип балаларга бирү өчен бер эзлекле, мәктәп
программасына карата ярдәмче яки өстәмә программага ихтыяҗы булган ата-аналар
һәм укытучылар;}

@li{төрле сәбәпләрдән балалары өйдә белем алучы гаиләләр;}

@li{урта мәктәпне бетерүгә үк югары уку йортына кермәгән, ә хәзер мәктәп курсын
кабатлап, үз ватанындамы, чит илдәме югары уку йортына укырга керергә
әзерләнүчеләр.}}

@p{Хәзер кыскача бу дүрт төркемнең мәгарифкә бәйле ихтыяҗларына һәм алар алдында
торган төп кыенлыкларга безнең караш.}

@subsection{Үз ватаннарыннан читтә яшәүче гаиләләр}

@p{Үз ватаннарыннан читтә яшәүче зур диаспораларның мисаллары байтак. Әйтик,
татарларның яртысыннан азрагы гына Татарстанда яши; 30 миллион русияленең
Русиядән читтә яшәве
билгеле@numbered-note{@hyperlink["https://www.interfax.ru/interview/131938"]{
Интерфакс агентлыгы, 2010 ел.}}; иран халкы диспорасы 5 миллионга якын һ.б.}

@p{Торган саен күбрәк глобальләшә баручы һәм, Аурупа Берлегендәге кебек, ил чикләре
юыла баручы дөньяда бер илдән икенче илгә күчеп китү күренешләре арта гына
барырга охшый. Тарихи ватаннарыннан еракта яшәүче ата-аналарның зур өлешендә
балаларының туган телләреннән өйдә аралашу теле буларак кына файдаланып
калмыйча, әлеге телдә хатасыз язарга өйрәнеп, ата-аналары үзләштергән мәдәни
мирасны да (тарих, әдәбият һ.б.) үзләштерүен күрү теләге бар. Шулай да, андый
теләге булган гаиләләрнең, үзеннән үзе аңлашылганча, гадәттә балаларын ана
телләренә өйрәтүче (якшәмбе) мәктәбендә укыту мөмкинлеге булмый. Балаларын
тарихи ватаннарындагы, тулы көн укытучы мәктәпләргә исәпләнгән уку-укыту
программасы буенча укыту да мөмкин эшкә охшамый. Ул мәктәпләр параллель рәвештә
унлап фәннән белем бирә, һәм һөнәри укытучыдан башка күз алдына
китергесез. Ата-аналарның бу эшкә вакыты да, белеме дә, түземлеге дә җитмәячәк,
ә инде бер мәктәпкә йөрүче баланы әйтеп торасы да юк. Ата-атаналары йөрәкләрендә
йөрткән әдәбият әсәрләрен, җырларны, ата-аналары яхшы белгән тарихи вакыйгаларны
балаларының белмәве бик кызганыч күренеш, безнеңчә, чөнки бу мәдәни мирасның,
аерым бер гаилә эчендә генә булса да, әкренләп югалуын аңлата.}

@p{Безнең уйлавыбызча, бу төркем гади, ягъни педагогик белеме булмаган ата-ана да
гамәлгә ашырырлык, уку-укыту программасына мохтаҗ.}

@subsection{Өстәмә яки ярдәмче программага ихтыяҗы булганнар}

@p{Икенче төркемгә килгәндә исә, без бүгенге көндә күргән мәктәпләрдә бер сыйныфта
укучы балаларның һәр теманы бер үк тизлектә үзләштерергә мәҗбүр булуларын
билгеләп үтәргә кирәк. Әдәбият кебек фәннәрдә, бер тема (әсәр) икенчесенә бик
ныкъ бәйләнгән булмаганлыктан, бу әллә ни кыенлыклар тудырмый. Әмма математика
кебек төгәл, һәр киләсе тема үткән темага нигезләнеп барган фәннәрдә бу
@emph{җитди} кыенлыклар тудыра. Чөнки еш кына алда тасвирланачак хәлләр
күзәтелә. "Гади вакланмаларны@numbered-note{Мисалы: 2/3} тапкырлау" кебек бер
теманы уткәч, балалар гадәттә контроль эше эшли. Укучыларның кайсысы бу контроль
эшендә 5-ле ала, кайсысы 3-ле, кайсысы 2-ле. Аннан соң, инде һәр бала да бу
теманы яхшы үзләштерде дигән кыяфәт белән, бөтен сыйныф "гади вакланмаларны
бүлү" темасына күчә. Әмма контроль эшендә 3-ле яки 2-ле алган балалар өчен моның
мәгънәсе юк, һәм, бер дәрестән соң ук ("хаталар өстендә эш") яңа тема
башланганлыктан, алар контроль эштә начар билге алган теманы ныкълап
үзләштеререргә тырышып карау мөмкинлегеннән мәхрүм калалар.}

@p{Метафора рәвешендә, белем алуны кирпечтән йорт төзү белән чагыштырырга рөхсәт
итсәгез, бу процесс әле ныгып өлгермәгән фундаментка йортның беренче катын өеп
куюны хәтерләтә. Яисә тишекләр калдырып өелгән беренче катка икенче катны өеп
кую. Еш кына, 9 нчы - 10 нчы "катка" җиткәндә (ягъни математик анализны өйрәнә
башлаганда), алдагы елларда җыелган "тишекләрнең" саны шундый күп була ки,
чираттагы катның кирпечләрен бөтенләй өстәп куеп булмый башлый -- бала
калькулюсны чынлап торып үзләштерә алмаслык хәлдә була.}

@p{Бу проблема, инде 60 нчы еллар азагында ук --Бенджамин Блум'ның "mastery
learning" теориясе кебек теорияләрдә-- билгеләнгән булса да, аның чишелеше ачык
булып кала бирә. Монда укытучыларны да гаепләргә ярамый, чөнки 25-ләп укучыдан
торучы сыйныфларда әлегә кадәр тәкъдим ителгән чишелешләр гамәлгә ашырырлык
булмаган, күрәсең. "Артта калучылар өйдә күбрәк шөгыльләнсеннәр" дип, гаепне
укучыларның үзләренә генә сылтап калдырга да ашыкмас идек. Беренчедән, һәр
баланың темадан темага (математик) белемне үзләштерү тизлеге даими булмаска
мөмкин. Башкача әйткәндә, бер тема җиңел бирелә балага, икенчесе
кыенрак. Бер-ике дәрестән яңа тема башланырга тиеш булганга, өй эшен эшләүгә
сарыф итә алган вакытын өй эшен эшләүгә сарыф иткән очракта да, ул кайчагында
"куып тота" алмаска мөмкин. Икенчедән, математиканы өйрәнә башлаганда баланың
чагыштырмача әкрен алга баруы аның киләчәктә начар математик булуын аңлатмаска
да мөмкин. Һәр теманы озаграк, әмма төплерәк итеп, мәсьәләләрне чишмичә
калдырмыйча, алдагы теманың киләсе тема белән бәйләнешен өзмичә белем алучы бала
ерак арада үз яшьтәшләреннән белеме буенча көчлерәк булып чыгарга да мөмкин.}

@p{"Укыйсы килмәсә укымый инде бала" дип, я "бер аңлый алмаса аңламый инде ул бу
теманы" дип кул селтәгәнче, чынлыкта @emph{укыйсы килеп тә}, артта калудан
күңеле кайтып, инде "мин моны гомер аңламаячакмын" дип үз-үзен дә ышандырган
укучыга тагын бер мөмкинлек бирү үтә мөһим, безнең уйлавыбызча.}

@p{Әлбәттә, мондый мөмкинлекне гадәттә репетиторлар бирә. Әмма репетитор ялларга
финанс ягынннан кыенсынган гаиләләр дә бар, ә сыйфатлы белем алу мөмкинлеге
һәркем кулында да булмыйча, акчалы кешеләр кулында гына булса, бу милләт үсешен
дә, дәүләт үсешен дә, гомумән, кешелек үсешен дә нык чикләр иде.
@numbered-note{"Минем Сезгә бер соравым бар. Дөньяда яшәгән Эйнштейннарның
ничәсенең физиканы өйрәнү мөмкинлеге булган? Берничәсенең генә. Дөньяга килгән
Шекспирларның ничәсе укырга-язарга өйрәнми бу дөньядан кичкән? Барчасы да
диярлек. Бүген Җир йөзендә 7 миллиард кеше яши, шуларның 3 миллиарды ---
балалар; бүгенге көндә ничә Эйнштейнны әрәм итәчәкбез? Белемне һәркем өчен дә
ачык итү --- дөньяда инновацияләрне арттыруның һәм кешеләрнең тормышын
җиңеләйтүнең төп чарасы." (Эбен Моглен,
@hyperlink["https://tt.wikipedia.org/wiki/Колумбия_университеты"]{Колумбия
университетында} хокук һәм хокук тарихы профессоры,
(@hyperlink["../tat/about.html#moglen"]{"Innovation under Austerity"}))@br{}
--- --- --- --- --- Инглизчә оригиналы: --- --- --- --- ---@br{}
"I have a question for you. How many of the Einsteins who ever
lived were allowed to learn physics? A couple. How many of the Shakespeares who
ever lived, lived and died whithout learning to read and write?  Almost all of
them. With 7 billion people in the world right now, 3 billion of them are
children; how many Einsteins do you want to throw away today? The
universalization of access to knowledge is the single most important force
available for increasing innovation and human welfare on the planet."  (Eben
Moglen, professor of law and legal history at
@hyperlink["https://en.wikipedia.org/wiki/Columbia_University"]{Columbia
University}, (@hyperlink["../eng/about.html#moglen"]{"Innovation under Austerity"}))}}

@p{Чишелешләргә килгәндә исә, бу проблемаларны чишүнең ике юлын күрәбез, һәм икесе
дә яңалык түгел. Икесе дә асылда укучы өстәленә аңа мөстәкыйль шөгыльләнергә
мөмкинлек бирүче, дөньяда булган иң сыйфатлы, бушлай уку әсбапларын куюдан
гыйбарәт. Беренчесендә уку әсбаплары ролен традицион кәгазь китаплар үти. Аңа
бирелгән китап буенча укучы үзлегеннән, ягъни башка яшьтәшләренең билгеле бер
теманы үзләштерү тизлегеннән бәйсез рәвештә шөгыльләнә. Бер темага бирелгән
мәсьәләләрдә җибәрелгән хаталар саны 0-гә якынлашкач кына, укучы икенче темага
күчә. Һәм һәр яңа тема үткән теманы кабатлаудан башлана. Шушы рәвешле
мөстәкыйль белем алуны мөмкин итүче китап мисалы --- Saxon Math китаплары.}

@p{Икенче чишелеш --- беренчесенең компьютерлаштырылган, ягъни автоматлаштарылган
варианты. Традицион кәгазь китапка алмашка, яки аңа өстәп, укучы алдан
әзерләнгән дәрес видеоларын карый ала. Моннан тыш, ул үз җавапларын компьютерга
кертә. Компьютер программасы укучы җибәргән хаталарны теркәп бара, һәм, шул
хаталарның саны һәм характерыннан чыгып --- machine learning методларын
кулланып, укучыга юнәлеш биреп тора@numbered-note{ә укытучыга --- укучының
көчле һәм зәгыйфь яклары, темадан аңа нәрсә аңлашылганы һәм нәрсә аңлашылмаганы
турында статистик мәгълүмат}. Моның мисалы:
"@hyperlink["https://www.khanacademy.org"]{Хан Академиясе}" исемле сайт.}

@p{Saxon Math китаплары, аларның сыйфатында һәм эчтәлекләре ягыннан безнең
уку-укыту программасыа туры килүендә шигебез булмаса да, күпчелек кеше өчен
артык кыйммәт, һәм копияләүне/үзгәртүне/тәрҗемә итүне рөхсәт итмәүче лицензия
шартлары белән нәшер ителә. Өстәрәк әйтелгән сәбәпләргә күрә без исә
программабызда тәкъдим ителгән барлык материалларның да бушлай булуын теләдек,
шуңа күрә Хан Академиясе материалларын тәрҗемә итәргә булдык. Алары бушлай һәм
тәрҗемә итүне, башкаларга таратуны рөхсәт итә. Тора-бара берәр нәшрият Saxon
Math китапларын да инглизчәдән тәрҗемә итәр дигән өметтә калабыз.}

@p{Ни генә булмасын, төп мәсьәлә --- укучыга һәр тема буенча, бигрәк тә аңа кыен
бирелгән темалар буенча, гамәлдә чиксез санда яңа мәсьәләр һәм аңа үзенең
җавабын үзе тикшерү мөмкинлеген бирү. Компьютер куллануга нигезләнгән курс,
асылда, башка бик күп очраклардагы кебек, бу эшне җиңеләйтә генә -- чөнки
җавапны автоматик рәвештә тикшерә һәм җаваплардан чыгып укучыга да, аның эшенә
күз-колак булып торучы укытучыга я ата-анага да статистик мәгълүмат бирә ала.}

@p{Бердәнбер күңелебезне бимазалаучы мәсьәлә --- башлангыч сыйныф яшендәге баланы
өстәл өсте компьютеры алдына утырту яки аңа смартфон я планшет тоттыру
сәламәтлегенә зыянлымы-юкмы икәне... Хәзерге заманда бу бик киң таралган
күренеш, әмма смартфоннарга бәйлелекне наркотикларга бәйлелек белән
чагыштыручы, һәм бу күренешнең балаларда торган саен ешрак күзәтелүе хакында
белдерүче фәнни тикшеренүләр
бар@numbered-note{@hyperlink["https://www.independent.co.uk/news/education/education-news/child-smart-phones-cocaine-addiction-expert-mandy-saligari-harley-street-charter-clinic-technology-a7777941.html"]{Giving
your child a smartphone is like giving them a gram of cocaine, says top
addiction
expert}}@sup{,}@numbered-note{@hyperlink["https://www.irishtimes.com/news/world/europe/child-smartphone-addiction-growing-says-german-drug-agency-1.3101529"]{Child
smartphone addiction growing, says German drug agency}}. Бу мәсьәләне истән
чыгармау кирәк. Киңәшебез --- сабыйлар selimcan.org'тагы китапларның электрон
версиясен түгел, ә кәгазьгә бастырылган версиясен укысын@numbered-note{Соңрак
бу сайтның һәр битендә, өске уң чатта "PDF" дигән төймә пәйда булачак. Аңа
басып, ул битнең гүзәл рәвештә эшләнгән PDF версиясен йөкләп алып, кәгазьгә
төшерә алачаксыз.}. Хан Академиясе видео һәм күнегүләренә килгәндә исә, алары
өчен шул сайтка гына чыга алучы аерым, мөмкинлекләре чикләнгән бер
смартфон/планшет/өстәл компьютеры я телевизор & клавиатура & "тычкан"га
тоташтырылган Raspberry Pi ише кесә компьютеры бер чишелеш була ала. Шул ук
кесә компьютерын соңыннан беренче программалау дәресләре өчен дә кулланып
булачак.}
