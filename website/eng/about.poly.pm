#lang pollen

@(define-meta title            "About selimcan.org")
@(define-meta doc-publish-date "2019-05-19")

@p{@smallcaps{Our mission is to help you learn. Our vision is to see students who
have completed our curriculum demonstrate knowledge and skills necessary to
study in top universities of the world.}}

@p{Hello! The goal of the selimcan.org project is to develop a free/gratis
online curriculum consisting of:}

@ol{
  @li{a reading and spelling course;}
  @li{a math course covering mathematics from arithmetic to calculus;}
  @li{a list of books which, in our opinion, anyone should have read by the time
  they finish high-school; original texts of these books along with their
  translations; question or problem sets to test reading comprehension;}
  @li{an explanatory dictionary;}
  @li{an encyclopedia, and}
  @li{a book teaching basics of computer program design,}}

@p{assembled prererably from materials published under free/libre licenses, and
mainly targeted at self-learners.}
    
@p{We plan to do the following:}

@ul{
  @li{developing lesson plans a-la Siegfried Engelmann's "Teach Your Child to Read
  in 100 Easy Lessons" for teaching basic reading and writing skills;}
  @li{translating the "math" section of @hyperlink["https://khanacademy.org"]{Khan Academy}}
  @li{selecting books which, in our opinion, anyone should have read by the time
  they finish high-school; preparing question/problem sets for these books;
  translating them if necessary;}
  @li{creating explanatory dictionaries on @hyperlink["https://wiktionary.org"]{wiktionary.org};}
  @li{translating selected articles of English Wikipedia;}
  @li{translating the book on program design at @hyperlink["https://htdp.org"]{htdp.org} (if we'll be able to obtain a permission to do so); translating the course at @hyperlink["http://www.appinventor.org"]{appinventor.org} on Android Development;}
  @li{localizing the @hyperlink["https://www.debian.org"]{Debian}
      @hyperlink["https://www.gnu.org"]{GNU/Linux} operating system.}}

@p{If you have any questions or feedback or want to help, please write to ‘contact
at selimcan dot org’.}

@hline{}

@div[#:id "moglen"]{
@p{One of the talks which inspired us to start this project:}

@amaravideo["http://www.youtube.com/watch?v=G2VHf5vpBy8"]{}

}

@hline{}

@section{More information}

@ul{
  @li{@hyperlink["manual.html#motivation"]{selimcan.org уку-укыту программасы
      нигә кирәк?}}
  @li{@hyperlink["manual.html#procedure"]{Эш тәртибе}}
  @li{@hyperlink["manual.html#advantages"]{Бу уку-укыту программасының өстенлекләре}}
  @li{@hyperlink["manual.html#start"]{Нидән башларга?}}
  @li{@hyperlink["manual.html#invest"]{Балаларыгызның киләчәгенә инвестиция кылыгыз}}  
}
