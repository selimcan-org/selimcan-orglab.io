#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "Selimcan.org — free online educational resources")
@(define-meta author           "Ilnar Salimzianov")

@h2{Contents}

@ol{

@li{@hyperlink["../eng/about.html"]{About selimcan.org}}
@li{@hyperlink["../tat/manual.html"]{Frequently Asked Questions about selimcan.org and the user guide}}
@li{English school}

@ol{

@li{@hyperlink["../tat/math/index.html"]{Mathematics}}
@li{Writing}
@li{Reading}}}
