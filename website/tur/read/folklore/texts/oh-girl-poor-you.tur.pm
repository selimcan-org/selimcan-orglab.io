#lang pollen

@h3{Ah Kız Sana Yazık [1]}

@p{Bir varmış, bir yokmuş. Evvel zaman içinde, kalbur saman içindeyken bir
adamın bir tek kızı varmış. Kız çeşmeye suya gidince çeşmenin oradaki kurbağa,
kıza:
    
- Ah kız sana yazık, vah kız sana yazık, dermiş. Bir gün sormuş kurbağaya:}

@p{- Yazık ama benim neyime yazık, demiş. O da kıza:}

@p{- Kırk gün ölü başı bekleyeceksin, ona yazık, demiş.}

@p{Bir gün kız öte dağın başında pınarın dibinde oynuyormuş. Derken kapı
açılmış ve bir babayiğit gelmiş. Kız içeri girince kapıları kilitlemiş. Allah
ta- rafından kırk gün beklemiş. Kız yıkanmaya gitmiş. O sırada elekçiler
gelmiş.  Elekçilerin arasındaki bir topal kız:}

@p{- Ah beni de yanına al. Ondan sonra kapı kilitlensin, demiş.}

@p{O kız, uyuyan babayiğidin başına oturmuş. O sırada adam uyanmış. Elekçi
kız:}

@p{- Ben senin kırk gündür başını bekliyorum, demiş.}

@p{Bunun üzerine elekçi kızla babayiğit evlenmiş. Diğer kız dünya güzeliymiş.
Bir gün böyle, beş gün böyle… Babayiğit bir gün şehre gidecekmiş. Elekçi kıza
sormuş:}

@p{- Ne alayım sana? Kız da:}

@p{- Esvap, altın, bilezik; demiş. Diğer kıza sormuş:}

@p{- Sana ne alayım? O da:}

@p{- Sabır taşıyla, sabır bıçağı al. Eğer almazsan yoluna boz duman çöke,
demiş.}

@p{Oğlan, elekçi kızın istediklerini almış. Sabır taşıyla sabır bıçağını almayı
unutmuş. Yoluna boz duman çökmüş, tekrar dönüp almış. Kıza sabır taşıyla sabır
bıçağını vermiş. Kız, sabır taşıyla sabır bıçağını alıp içeri gitmiş, ağlamış.}

@p{Kendi kendine söyleniyormuş:}

@p{- Ben zamanında anamın, babamın bir kızıydım. Bir kurbağa bana, “Ah kız sana
yazık, vah kız sana yazık. Kırk gün ölü başı bekleyeceksin, ona yazık”
demişti.}

@p{Oğlan, kızın bu söylediklerini hep dinlemiş. Kız sabır bıçağını kalbine sap-
layacakken delikanlı hızla koşup kızın kolunu tutmuş. Oğlan, kıza:}

@p{- Niye bana gerçeği söylemedin, demiş.}

@p{Elekçi kızı atın kuyruğuna bağlamış, salmış. Böylece yiyip, içip muratlarına
ermişler.}

@p{Onlar ermiş muradına, biz çıkalım kerevetine…}

@p{Hatice ŞEN}
