#lang pollen

@h3{Ahmet ile Mahmut [1]}

@p{Vaktin birinde, bir padişah varmış. Padişahın bir sene, beş sene derken epey
bir zaman çocuğu olmamış. Olmayınca padişahın hanımı:
    
- Padişahım; her derdin dermanı var, derdine derman arasana. Sen padişahsın.}

@p{Padişah; vezirini, vüzerasını, akıldanelerini toplamış.}

@p{- Benim bir sürü serim servetim var. Ben ölünce bu servet nerede kalır?
Benim çocuğum yok. Derdi veren Allah, dermanını da verir, çıkalım arayalım.}

@p{Bunlar, atlarını hazırlamışlar. Yakınları ile vedalaşıp yola çıkmışlar. Bir
müddet gittikten sonra çayırlık, çimenlik bir yere varmışlar. Atları çayıra
salmışlar. Bunlar da çeşmenin başına varmışlar. Abdest alıp namaza durmuşlar.}

@p{Padişah namazının sonunda sağına selam vermiş, soluna selam vereceği zaman
bir sakallı adam gelmiş.}

@p{- Merhaba, padişahım.}

@p{- Merhaba derviş baba. Sen benim padişah olduğumu bildin, kalbimdekini de
bilirsin.}

@p{- Elbette biliyorum. Senin çocuğun olmuyor, derdine derman aramaya
çıktınız.}

@p{- Madem derdimi biliyorsun, sen dermanını da bilirsin.}

@p{- Tabiî, onu da biliyorum, demiş derviş ve cebinden bir elma çıkarıp ikiye
bölmüş. Derviş:}

@p{- Bunun yarısını sen yiyeceksin, yarısını hanımın yiyecek. Sizin iki tane
çocuğunuz olacak. Biri sizin, biri benim. Bu kavle razı mısın?}

@p{- Razıyım. Allah, iki tane oğlan çocuğu versin de biri senin biri benim
olsun.}

@p{Dervişle padişah anlaşmışlar. Sonra da derviş, padişaha veda edip oradan
ayrılmış.}

@p{Padişah ve yanındakiler geri dönmüşler. Geldiklerinde padişahın hanımı
sormuş:}

@p{- Ne yaptınız, derde derman bir şey bulabildiniz mi?}

@p{Padişah, olanları hanımına anlatmış. O gece elmayı hanımıyla yiyip
yatmışlar.}

@p{Zaman gelmiş, padişahın iki oğlu olmuş. Padişahın keyfine, mutluluğuna
diyecek yokmuş. Bu çocuklar ayda büyüyeceğine, günde büyümüşler ve okul çağına
gelmişler. Çocuklar okul çağına gelmişler ama daha adları konulmamış.
Çocuklar:}

@p{- Baba bizim adımızı koysanıza.}

@p{Derviş, elmayı padişaha verdiği gün:}

@p{- Ben gelmeden çocukların adını koymayın, diye söylemiş.}

@p{Çocuklara, hep “adsız gel, adsız git” derlermiş.}

@p{Bir ihtiyar nine varmış. Bu nine elinde testiyle çeşmeden geliyormuş. Bu
çocuklar bir ok atmışlar, ihtiyar kadının elindeki testiyi delmişler. Nine
kadın bakmış ki testiden şırıl şırıl su akıyor. Geriye dönüp bakmış ki oku atan
padişahın oğlu. Nine, kızgınlıkla:}

@p{- Zaten iyi adam değilsiniz ki, zaten iyi adam oğlu olsanız adınız
olurdu. Siz p... siniz, demiş. Bu sefer oğlan eve gelmiş. Annesine:}

@p{- Anne, biz p… mişiz.}

@p{- Kim dedi bunu size?}

@p{- Falanca nine kadın. Biz p… olmasak adımızı koyarsınız.}

@p{- Yok! Siz padişahın oğlusunuz.}

@p{- Yok anne, ya bizim adımızı koyarsınız ya da seni öldürürüz.}

@p{Neyse akşam olmuş, padişah eve gelmiş.}

@p{- Padişahım sağ olsun! Bugün çocuklara adları yok diye p... demişler. Bu
çocukların adını niye koymuyoruz? Çocuklar beni öldürecekler yoksa.}

@p{- Yahu hanım, nasıl adlarını koyalım? Derviş baba, “Ben gelinceye kadar
adlarını koymayın” dedi.}

@p{- Aradan yıllar geçti, işte gelmiyor...}

@p{- Ne yapalım o zaman?}

@p{- Ziyafetini yap, yemeğini hazırla, adamlarını topla, çocukların adlarını
koyalım.}

@p{Kazanlar kurulmuş, yemekler pişirilmiş. Bütün iş bittikten sonra sıra
çocukların adını koymaya gelmiş. Orada bulunanlar saraya geçmişler.}

@p{- Bunların adı ne olsun, demişler.}

@p{Biri “Ahmet”, biri “Mehmet”, biri “Muhammed”, diyormuş. Her biri bir şey
söylüyormuş. Bunlar böyle isim bulmaya çalışırken derviş çıkagelmiş. Padişah;
dervişi görünce tanımış, karşılamış, başköşeye oturtmuş.}

@p{- Arkadaşlar! Bu çocuklar derviş babanın himmeti, bunların adını derviş baba
koyar.}

@p{- Hayırdır. Bir hayır işiniz mi var?}

@p{- Derviş baba bu çocuklar kemale erdi. Çocukların adı yok, bunlara p…
diyorlar. Bunları köye sığdırmıyorlar, çocukların adını koyacaktık sen geldin,
sen koyacaksın adlarını.}

@p{- Bu çocukların birinin adı Mahmut, birinin adı Ahmet.}

@p{- Otur bakalım, derviş baba.}
