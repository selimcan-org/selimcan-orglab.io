#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "Selimcan.org — тегін онлайн оқу-оқыту аспаптары")
@(define-meta author           "Илнар Сәлимҗанов")

@h2{Мазмұн}

@ol{

@li{@hyperlink["#../kaz/about.html"]{Selimcan.org туралы}}
@li{@hyperlink["#../eng/manual.html"]{Selimcan.org оқу-оқыту программасы туралы берілетін кейбір сұрақтар және программаны қолдану тәртібі}}
@li{Ағылшын мектебі}

@ol{

@li{@hyperlink["#../tat/math/index.html"]{Математика}}
@li{Жазу}
@li{Оқу}}}
