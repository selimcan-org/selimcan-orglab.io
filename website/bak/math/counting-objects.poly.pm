#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Counting objects} @tat{Кешеләрҙе һанау}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/--ZIp7bzQVo"
         #:english "https://www.youtube-nocookie.com/embed/leDYnoNSvD4"
         #:tatar "https://www.youtube-nocookie.com/embed/leDYnoNSvD4"]{@video-title{@u{@eng{Counting in pictures} @tat{Рәс[ге заттарҙы] һанау}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-count-object-topic/e/counting-in-scenes" #:target "_blank"]{@practice-title{@u{@eng{Practice: Count in pictures} @tat{Күнегеү: рәсемдә[ге заттарҙы] һана}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/KDF8ZKJQVEA"
         #:english "https://www.youtube-nocookie.com/embed/I9S5CVSQB5A"
         #:tatar "https://www.youtube-nocookie.com/embed/I9S5CVSQB5A"]{@video-title{@u{@eng{Counting objects 1} @tat{Кешеләрҙе [әйбер йә йән ияһыларын] һанау 1}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-count-object-topic/e/how-many-objects-1" #:target "_blank"]{@practice-title{@u{@eng{Practice: Count objects 1} @tat{Күнегеү: заттарҙы [әйбер йә йән ияһыларын] һана 1}}}}}
