#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "")
@(define-meta author           "")

@section{@u{@eng{Numbers 0 to 120} @tat{0-дән алып 120-гә кадәрге һандар}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-numbers-120/e/count-from-any-number" #:target "_blank"]{@practice-title{@u{@eng{Practice: Missing numbers} @tat{Күнегеү: төшөп ҡалған һандар}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/2VpsRGKRo7o"
       #:english "https://www.youtube-nocookie.com/embed/9XZypM2Z3Ro"
       #:tatar "https://www.youtube-nocookie.com/embed/9XZypM2Z3Ro"]{@video-title{@u{@eng{Number grid} @tat{Һандар йәҙүәле}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/TS3I1W8RLJY"
       #:english "https://www.youtube-nocookie.com/embed/1AqkBdCBm9o"
       #:tatar "https://www.youtube-nocookie.com/embed/1AqkBdCBm9o"]{@video-title{@u{@eng{Missing numbers between 0 and 120} @tat{0 менән 120 араһында төшөп ҡалған һандар}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-numbers-120/e/count-to-100"
                #:target "_blank"]{@practice-title{@u{@eng{Practice: Numbers to 100} @tat{Күнегеү: 100-гә кадәрге һандар}}}}}

@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-numbers-120/e/numbers-to-120"
                #:target "_blank"]{@practice-title{@u{@eng{Practice: Numbers to 120} @tat{Күнегеү: 120-гә кадәрге һандар}}}}}

@kavideo[#:frank "https://www.youtube-nocookie.com/embed/UNPPFW3NATI"
       #:english "https://www.youtube-nocookie.com/embed/UNPPFW3NATI"
       #:tatar
       "https://www.youtube-nocookie.com/embed/UNPPFW3NATI"]{@video-title{@u{@eng{Count
       by tens} @tat{Унарлап һана}}}}
	     	     
@practice{@a[#:href "https://www.khanacademy.org/math/early-math/cc-early-math-counting-topic/cc-early-math-numbers-120/e/counting-tens"
                #:target "_blank"]{@practice-title{@u{@eng{Practice: Count tens} @tat{Күнегеү: тиҫтәләрҙе һана}}}}}
