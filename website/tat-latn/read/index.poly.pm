#lang pollen

@(define-meta template         "template.html")
@(define-meta title            "Татарча укырга өйрәнәбез")
@(define-meta author           "")

@ul[#:style "color: green; font-size: 60px; list-style-type: none; display: grid; grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));"]{

 @li[#:style "border: 2px solid blue; text-align: center"]{A a}
 @li[#:style "border: 2px solid blue; text-align: center"]{Ä ä}
 @li[#:style "border: 2px solid blue; text-align: center"]{B b}
 @li[#:style "border: 2px solid blue; text-align: center"]{C c}
 @li[#:style "border: 2px solid blue; text-align: center"]{Ç ç}
 @li[#:style "border: 2px solid blue; text-align: center"]{D d}
 @li[#:style "border: 2px solid blue; text-align: center"]{E e}
 @li[#:style "border: 2px solid blue; text-align: center"]{F f}
 @li[#:style "border: 2px solid blue; text-align: center"]{G g}
 @li[#:style "border: 2px solid blue; text-align: center"]{Ğ ğ}
 @li[#:style "border: 2px solid blue; text-align: center"]{H h}
 @li[#:style "border: 2px solid blue; text-align: center"]{I ı}
 @li[#:style "border: 2px solid blue; text-align: center"]{İ i}
 @li[#:style "border: 2px solid blue; text-align: center"]{J j}
 @li[#:style "border: 2px solid blue; text-align: center"]{K k}
 @li[#:style "border: 2px solid blue; text-align: center"]{Q q}
 @li[#:style "border: 2px solid blue; text-align: center"]{L l}
 @li[#:style "border: 2px solid blue; text-align: center"]{M m}
 @li[#:style "border: 2px solid blue; text-align: center"]{N n}
 @li[#:style "border: 2px solid blue; text-align: center"]{Ñ ñ}
 @li[#:style "border: 2px solid blue; text-align: center"]{O o}
 @li[#:style "border: 2px solid blue; text-align: center"]{Ö ö}
 @li[#:style "border: 2px solid blue; text-align: center"]{P p}
 @li[#:style "border: 2px solid blue; text-align: center"]{R r}
 @li[#:style "border: 2px solid blue; text-align: center"]{S s}
 @li[#:style "border: 2px solid blue; text-align: center"]{Ş ş}
 @li[#:style "border: 2px solid blue; text-align: center"]{T t}
 @li[#:style "border: 2px solid blue; text-align: center"]{U u}
 @li[#:style "border: 2px solid blue; text-align: center"]{Ü ü}
 @li[#:style "border: 2px solid blue; text-align: center"]{V v}
 @li[#:style "border: 2px solid blue; text-align: center"]{W w}
 @li[#:style "border: 2px solid blue; text-align: center"]{X x}
 @li[#:style "border: 2px solid blue; text-align: center"]{Y y}
 @li[#:style "border: 2px solid blue; text-align: center"]{Z z}
    
}
